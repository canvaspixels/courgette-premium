# Available Step Definitions

Note that the words in italics are optional.

## Step definitions that _don’t_ require page objects to work

### Given...

| Step definition | Snippet Code | Notes |
| --- | --- | --- |
| Given I am on the page with url 'URL' | givengotourl | Goes to a page by URL |
| Given the page url is 'URL' | givenpageurl | Checks the page url |
| Given the page url is not 'URL' | givennotpageurl |  |
| Given the page url contains 'URL' | givenurlcontains | Checks the page url contains |
| Given animations are disabled | givendisableAnimations | Disables CSS animations |
| Given _the_ _'NTH'_ element with xpath 'XPATH' is visible | givenvisiblexpath |  |
| Given _the_ element with selector 'SELECTOR' is visible | givenvisibleselector |  |
| Given _the_ _'NTH'_ element with xpath 'XPATH' is hidden | givenhiddenxpath |  |
| Given _the_ element with selector 'SELECTOR' is hidden | givenhiddenselector |  |
| Given _the_ _'NTH'_ element with xpath 'XPATH' is enabled | givenenabledxpath |  |
| Given _the_ element with selector 'SELECTOR' is enabled | givenenabledselector |  |
| Given _the_ _'NTH'_ element with xpath 'XPATH' is disabled | givendisabledxpath |  |
| Given _the_ element with selector 'SELECTOR' is disabled | givendisabledselector |  |
| Given _the_ _'NTH'_ element with xpath 'XPATH' is selected | givenselectedxpath |  |
| Given _the_ _'NTH'_ element with xpath 'XPATH' is not selected | givennotselectedxpath |  |
| Given _the_ element with selector 'SELECTOR' is selected | givenselectedselector |  |
| Given _the_ element with selector 'SELECTOR' is not selected | givennotselectedselector |  |
| Given _the_ _'NTH'_ element with xpath 'XPATH' is checked | givencheckedxpath |  |
| Given _the_ _'NTH'_ element with xpath 'XPATH' is not checked | givennotcheckedxpath |  |
| Given _the_ element with selector 'SELECTOR' is checked | givencheckedselector |  |
| Given&nbsp;_the_&nbsp;element&nbsp;with&nbsp;selector&nbsp;'SELECTOR'&nbsp;is&nbsp;not&nbsp;checked | givennotcheckedselector |  |
| Given _the_ _'NTH'_ element with xpath 'XPATH' is on the page | givenexistsxpath |  |
| Given _the_ _'NTH'_ element with xpath 'XPATH' is not on the page | givennotexistsxpath |  |
| Given _the_ element with selector 'SELECTOR' is on the page | givenexistsselector |  |
| Given _the_ element with selector 'SELECTOR' is not on the page | givennotexistsselector |  |
| Given the title is 'STRING' | giventitle |  |
| Given the title is not 'STRING' | givennottitle |  |
| Given _the_ _'NTH'_ element with xpath 'XPATH' contains the text 'STRING' | givencontainstextxpath |  |
| Given _the_ element with selector 'SELECTOR' contains the text 'STRING' | givencontainstextselector |  |
| Given _the_ _'NTH'_ element with xpath 'XPATH' does not contain the text 'STRING' | givennotcontainstextxpath |  |
| Given _the_ element with selector 'SELECTOR' does not contain the text 'STRING' | givennotcontainstextselector |  |
| Given _the_ _'NTH'_ element with xpath 'XPATH' contains any text | givencontainsanytextxpath |  |
| Given _the_ element with selector 'SELECTOR' contains any text | givencontainsanytextselector |  |
| Given _the_ _'NTH'_ element with xpath 'XPATH' does not contain any text | givennotcontainsanytextxpath |  |
| Given _the_ element with selector 'SELECTOR' does not contain any text | givennotcontainsanytextselector |  |
| Given _the_ _'NTH'_ element with xpath 'XPATH' has an attribute 'ATTRIBUTE_NAME' with a value of 'VALUE' | givenattributexpath |  |
| Given _the_ element with selector 'SELECTOR' has an attribute 'ATTRIBUTE_NAME' with a value of 'VALUE' | givenattributeselector |  |
| Given _the_ _'NTH'_ element with xpath 'XPATH' is empty | givenemptyxpath |  |
| Given _the_ _'NTH'_ element with xpath 'XPATH' is not empty | givennotemptyxpath |  |
| Given _the_ element with selector 'SELECTOR' is empty | givenemptyselector |  |
| Given _the_ element with selector 'SELECTOR' is not empty | givennotemptyselector |  |
| Given the value of _the_ _'NTH'_ element with xpath 'XPATH' is 'VALUE' | givenvaluexpath |  |
| Given the value of _the_ _'NTH'_ element with xpath 'XPATH' is not 'VALUE' | givennotvaluexpath |  |
| Given the value of _the_ element with selector 'SELECTOR' is 'VALUE' | givenvalueselector |  |
| Given the value of _the_ element with selector 'SELECTOR' is not 'VALUE' | givennotvalueselector |  |
| Given I set the cookie 'COOKIE_NAME' with value 'VALUE' | givensetcookie |  |
| Given the cookie 'COOKIE_NAME' is set to 'VALUE' | givencookie |  |
| Given the cookie 'COOKIE_NAME' is not set to 'VALUE' | givennotcookie |  |
| Given the cookie 'COOKIE_NAME' is set | givencookieset |  |
| Given the cookie 'COOKIE_NAME' is not set | givennotcookieset |  |

### When...

| Step definition | Snippet Code | Notes |
| --- | --- | --- |
| When I append 'STRING' to _the_ _'NTH'_ element with xpath 'XPATH' | whenappendinputwithxpath |  |
| When I append 'STRING' to _the_ element with selector 'SELECTOR' | whenappendinputwithselector |  |
| When I set _the_ _'NTH'_ element with xpath 'XPATH' to 'STRING' | whensetinputwithxpath |  |
| When I set _the_ element with selector 'SELECTOR' to 'STRING' | whensetinputwithselector |  |
| When I append 'STRING' to _the_ _'NTH'_ react element with xpath 'XPATH' | whenappendreactinputwithxpath | Sets the value to the input then fires React’s version of the onChange event, so that any actions fire |
| When I append 'STRING' to _the_ react element with selector 'SELECTOR' | whenappendreactinputwithselector | Sets the value to the input then fires React’s version of the onChange event, so that any actions fire |
| When I set _the_ _'NTH'_ react element with xpath 'XPATH' to 'STRING' | whensetreactinputfieldvaluewithxpath |  |
| When I set _the_ react element with selector 'SELECTOR' to 'STRING' | whensetreactinputfieldvaluewithselector |  |
| When I clear _the_ _'NTH'_ element with xpath 'XPATH' | whenclearinputwithxpath |  |
| When I clear _the_ element with selector 'SELECTOR' | whenclearinputwithselector |  |
| When I select the option for _the_ _'NTH'_ select with xpath 'XPATH' with the text 'VALUE' | whenselectbyoptiontextxpath |  |
| When I select the option for _the_ _'NTH'_ select with selector 'SELECTOR' with the text 'VALUE' | whenselectbyoptiontextselector |  |
| When I wait 'VALUE' seconds? | whenwait | singular or plural works (second or seconds) |
| When I wait for _the_ _'NTH'_ select with xpath 'XPATH' to exist | whenwaitforelementwithxpath |  |
| When I wait for _the_ _'NTH'_ select with xpath 'XPATH' to not exist | whennotwaitforelementwithxpath |  |
| When I wait for _the_ element with selector 'SELECTOR' to exist | whenwaitforelementwithselector |  |
| When I wait for _the_ element with selector 'SELECTOR' to not exist | whennotwaitforelementwithselector |  |
| When I set the file upload 'VALUE' to _the_ element with selector 'SELECTOR' | whenuploadfileselector |  |
| When I click _the_ _'NTH'_ element with xpath 'XPATH' | whenclickelwithxpath |  |
| When I click _the_ element with selector 'SELECTOR' | whenclickelwithselector |  |
| When&nbsp;I&nbsp;click&nbsp;_the_&nbsp;_'NTH'_&nbsp;element&nbsp;with&nbsp;the&nbsp;text&nbsp;'VALUE' | whenclickelwithtext |  |
| When I click _the_ _'NTH'_ element that contains the text 'VALUE' | whenclickelcontainstext |  |
| When I press 'KEY' | whenkey | [See list of possible keys](https://gist.github.com/canvaspixels/a5793fe712743dda9216eef06cc96022) - [This only works in ChromeDriver](https://github.com/canvaspixels/courgette/issues/16) |

### Then...

| Step definition | Snippet Code | Notes |
| --- | --- | --- |
| Then I expect page to contain 'STRING' | thenpagecontainstext | This looks in the whole document for STRING |
| Then I expect the url to be 'STRING' | thenurl | Using this just checks the URL, it does not change the page object so should not be used for end to end testing unless it is the final step |
| Then I expect the url to not be 'STRING' | thennoturl |  |
| Then I expect the url to contain 'STRING' | thenurlcontains | Using this just checks the URL, it does not change the page object. |
| Then I expect the url 'URL' is opened in a new tab | thenurlnewtab | [Currently not working in FirefoxDriver](https://github.com/canvaspixels/courgette/issues/16) |
| Then I expect  _the_ _'NTH'_ element with xpath 'XPATH' to be visible | thenvisiblexpath |  |
| Then I expect  _the_ element with selector 'SELECTOR' to be visible | thenvisibleselector |  |
| Then I expect  _the_ _'NTH'_ element with xpath 'XPATH' to be hidden | thenhiddenxpath |  |
| Then I expect  _the_ element with selector 'SELECTOR' to be hidden | thenhiddenselector |  |
| Then I expect the colour of _the_ _'NTH'_ element with xpath 'XPATH' to be 'STRING' | thencolourxpath |  |
| Then I expect the colour of _the_ element with selector 'SELECTOR' to be 'STRING' | thencolourselector |  |
| Then I expect  _the_ _'NTH'_ element with xpath 'XPATH' to contain the text 'STRING' | thencontainstextxpath |  |
| Then I expect  _the_ element with selector 'SELECTOR' to contain the text 'STRING' | thencontainstextselector |  |
| Then I expect  _the_ _'NTH'_ element with xpath 'XPATH' to not contain the text 'STRING' | thennotcontainstextxpath |  |
| Then I expect  _the_ element with selector 'SELECTOR' to not contain the text 'STRING' | thennotcontainstextselector |  |
| Then I expect  _the_ _'NTH'_ element with xpath 'XPATH' to contain any text | thencontainsanytextxpath |  |
| Then I expect  _the_ _'NTH'_ element with xpath 'XPATH' to not contain any text | thennotcontainsanytextxpath |  |
| Then I expect  _the_ element with selector 'SELECTOR' to contain any text | thencontainsanytextselector |  |
| Then I expect  _the_ element with selector 'SELECTOR' to not contain any text | thennotcontainsanytextselector |  |
| Then I expect  _the_ _'NTH'_ element with xpath 'XPATH' to appear exactly 'NUMBER' times | thenappearexactlyxpath |  |
| Then I expect  _the_ _'NTH'_ element with xpath 'XPATH' to not appear exactly 'NUMBER' times | thennotappearexactlyxpath |  |
| Then I expect  _the_ element with selector 'SELECTOR' to appear exactly 'NUMBER' times | thenappearexactlyselector |  |
| Then I expect  _the_ element with selector 'SELECTOR' to not appear exactly 'NUMBER' times | thennotappearexactlyselector |  |
| Then I expect  _the_ _'NTH'_ element with xpath 'XPATH' to exist | thenexistsxpath |  |
| Then I expect  _the_ _'NTH'_ element with xpath 'XPATH' to not exist | thennotexistsxpath |  |
| Then I expect  _the_ element with selector 'SELECTOR' to exist | thenexistsselector |  |
| Then I expect  _the_ element with selector 'SELECTOR' to not exist | thennotexistsselector |  |
| Then I expect  _the_ _'NTH'_ element with xpath 'XPATH' to be checked | thencheckedxpath |  |
| Then I expect  _the_ _'NTH'_ element with xpath 'XPATH' to not be checked | thennotcheckedxpath |  |
| Then I expect  _the_ element with selector 'SELECTOR' to be checked | thencheckedselector |  |
| Then I expect  _the_ element with selector 'SELECTOR' to not be checked | thennotcheckedselector |  |
| Then I expect  _the_ _'NTH'_ element with xpath 'XPATH' to be selected | thenselectedxpath |  |
| Then I expect  _the_ _'NTH'_ element with xpath 'XPATH' to not be selected | thennotselectedxpath |  |
| Then I expect  _the_ element with selector 'SELECTOR' to be selected | thenselectedselector |  |
| Then I expect  _the_ element with selector 'SELECTOR' to not be selected | thennotselectedselector |  |
| Then I expect  _the_ _'NTH'_ element with xpath 'XPATH' to be enabled | thenenabledxpath |  |
| Then I expect  _the_ element with selector 'SELECTOR' to be enabled | thenenabledselector |  |
| Then I expect  _the_ _'NTH'_ element with xpath 'XPATH' to be disabled | thendisabledxpath |  |
| Then I expect  _the_ element with selector 'SELECTOR' to be disabled | thendisabledselector |  |
| Then I expect cookie 'COOKIE_NAME' to contain 'STRING' | thencookiecontain |  |
| Then&nbsp;I&nbsp;expect&nbsp;cookie&nbsp;'COOKIE_NAME'&nbsp;to&nbsp;not&nbsp;contain&nbsp;'STRING' | thennotcookiecontain |  |
| Then I expect cookie 'COOKIE_NAME' to exist | thencookieexists |  |
| Then I expect cookie 'COOKIE_NAME' to not exist | thennotcookieexists |  |
| Then I expect  _the_ _'NTH'_ element with xpath 'XPATH' to have the class 'CLASS_NAME' | thenclassnamexpath |  |
| Then I expect  _the_ _'NTH'_ element with xpath 'XPATH' to not have the class 'CLASS_NAME' | thennotclassnamexpath |  |
| Then I expect  _the_ element with selector 'SELECTOR' to have the class 'CLASS_NAME' | thenclassnameselector |  |
| Then I expect  _the_ element with selector 'SELECTOR' to not have the class 'CLASS_NAME' | thennotclassnameselector |  |
| Then I expect  _the_ _'NTH'_ element with xpath 'XPATH' to be empty | thenemptyxpath |  |
| Then I expect  _the_ _'NTH'_ element with xpath 'XPATH' to not be empty | thennotemptyxpath |  |
| Then I expect  _the_ element with selector 'SELECTOR' to be empty | thenemptyselector |  |
| Then I expect  _the_ element with selector 'SELECTOR' to not be empty | thennotemptyselector |  |
| Then I expect the value of _the_ _'NTH'_element with xpath 'XPATH' to be 'STRING' | thenvaluexpath | Used for getting the value of an input |
| Then I expect the value of _the_ _'NTH'_element with xpath 'XPATH' to not be 'STRING' | thennotvaluexpath |  |
| Then I expect the value of _the_ element with selector 'SELECTOR' to be 'STRING' | thenvalueselector | Used for getting the value of an input |
| Then I expect the value of _the_ element with selector 'SELECTOR' to not be 'STRING' | thennotvalueselector |  |
| Then I expect _the_ _'NTH'_element with xpath 'XPATH' has an attribute 'ATTRIBUTE_NAME' with a value of 'VALUE' | thenattributexpath |  |
| Then I expect _the_ element with selector 'SELECTOR' has an attribute 'ATTRIBUTE_NAME' with a value of 'VALUE' | thenattributeselector |  |
| Then I expect  _the_ _'NTH'_ element with xpath 'XPATH' has an attribute 'ATTRIBUTE_NAME' with a value of 'VALUE' | thenattributexpath |  |
| Then I expect  _the_ element with selector 'SELECTOR' has an attribute 'ATTRIBUTE_NAME' with a value of 'VALUE' | thenattributeselector |  |
| Then take a screenshot | thenscreenshot |  |
| Then take a screenshot called 'STRING' | thenscreenshotcalled |  |


## Step definitions that require page objects to work

### Given...

| Step definition | Snippet Code | Notes |
| --- | --- | --- |
| Given I am on the 'PAGE_NAME' page | givenonpage | PAGE_NAME should match the name of the page object file in your pages directory but use spaces instead of dashes and use lowercase for your page object file names with dash separating (kebab-case). This step definition sets the current page object |
| Given _the_ 'LOCATOR' is visible | givenvisible |  |
| Given _the_ 'LOCATOR' is hidden | givenhidden |  |
| Given _the_ 'LOCATOR' is enabled | givenenabled |  |
| Given _the_ 'LOCATOR' is disabled | givendisabled |  |
| Given _the_ 'LOCATOR' is selected | givenselected |  |
| Given _the_ 'LOCATOR' is not selected | givennotselected |  |
| Given _the_ 'LOCATOR' is checked | givenchecked |  |
| Given _the_ 'LOCATOR' is not checked | givennotchecked |  |
| Given _the_ 'LOCATOR' is on the page | givenexists |  |
| Given _the_ 'LOCATOR' is not on the page | givennotexists |  |
| Given _the_ 'LOCATOR' contains the text 'STRING' | givencontainstext |  |
| Given&nbsp;_the_&nbsp;'LOCATOR'&nbsp;does&nbsp;not&nbsp;contain&nbsp;the&nbsp;text&nbsp;'STRING' | givennotcontainstext |  |
| Given _the_ 'LOCATOR' contains any text | givencontainsanytext |  |
| Given _the_ 'LOCATOR' does not contain any text | givennotcontainsanytext |  |
| Given _the_ 'LOCATOR' has an attribute 'ATTRIBUTE_NAME' with a value of 'VALUE' | givenattribute |  |
| Given _the_ 'LOCATOR' is empty | givenempty |  |
| Given _the_ 'LOCATOR' is not empty | givennotempty |  |
| Given the value of _the_ 'LOCATOR' is 'VALUE' | givenvalue |  |
| Given the value of _the_ 'LOCATOR' is not 'VALUE' | givennotvalue |  |

### When...

| Step definition | Snippet Code | Notes |
| --- | --- | --- |
| When I wait for _the_ 'LOCATOR' to exist | whenwaitforelement |  |
| When I wait for _the_ 'LOCATOR' to not exist | whennotwaitforelement |  |
| When&nbsp;I&nbsp;set&nbsp;the&nbsp;file&nbsp;upload&nbsp;'VALUE'&nbsp;to&nbsp;_the_&nbsp;'LOCATOR' | whenuploadfile |  |
| When I click _the_ 'LOCATOR' | whenclick |  |
| When I click _the_ 'LOCATOR' inside _the_ 'LOCATOR' | whenclickelinsideel | This currently only works with XPaths |
| When I append 'STRING' to 'LOCATOR' | whenappend |  |
| When I set 'LOCATOR' to 'STRING' | whenset |  |
| When I set _the_ 'LOCATOR' inside _the_ 'LOCATOR' to 'STRING' | whensetelinsideel | This currently only works with XPaths |
| When I append 'STRING' to react field 'LOCATOR' | whenappendreact | Sets the value to the input then fires React’s version of the onChange event, so that any actions fire |
| When I set react field 'LOCATOR' to 'STRING' | whensetreact | Similar to append in react above |
| When I submit the _form_ 'LOCATOR' | whensubmit | [This only works in ChromeDriver](https://github.com/SeleniumHQ/selenium/issues/4359) |
| When I clear _the_ 'LOCATOR' | whenclear |  |
| When I select the option for select element 'LOCATOR' with the text 'VALUE' | whenoption |  |

### Then...

| Step definition | Snippet Code | Notes |
| --- | --- | --- |
| Then I expect to be on the 'PAGE_NAME' page | thenonpage | This step does 2 things: it changes the current page object so that any subsequent steps will use locators / selectors / XPaths from the PAGE_NAME page object, and then asserts the URL from that new page object if it exists. |
| Then I set the page object to 'PAGE_NAME' page | thensetpageobj | This changes the current page object so that any subsequent steps will use locators / selectors / XPaths from the PAGE_NAME page object |
| Then I expect _the_ 'LOCATOR' to be visible | thenvisible |  |
| Then I expect _the_ 'LOCATOR' inside _the_ 'LOCATOR' to be visible | thenelinsideelvisible | This currently only works with XPaths |
| Then I expect _the_ 'LOCATOR' to be hidden | thenhidden |  |
| Then I expect the (bottom OR top OR left OR right)* border colour of the 'LOCATOR' to be 'STRING' | thenbordercolour | Pick a side (bottom, top, left, or right) or remove the expected side. |
| Then&nbsp;I&nbsp;expect&nbsp;the&nbsp;colour&nbsp;of&nbsp;_the_&nbsp;'LOCATOR'&nbsp;to&nbsp;be&nbsp;'STRING' | thencolour |  |
| Then I expect the background colour of the 'LOCATOR' to be 'STRING' | thenbackgroundcolour |  |
| Then I expect the title to be 'STRING' | thentitle |  |
| Then I expect the title to not be 'STRING' | thennottitle |  |
| Then I expect _the_ 'LOCATOR' to contain the text 'STRING' | thencontainstext |  |
| Then I expect _the_ 'LOCATOR' to not contain the text 'STRING' | thennotcontainstext |  |
| Then I expect _the_ 'LOCATOR' inside _the_ 'LOCATOR' to contain the text 'STRING' | thenelinsideelcontainstext |  |
| Then I expect _the_ 'LOCATOR' to contain any text | thencontainsanytext |  |
| Then I expect _the_ 'LOCATOR' to not contain any text | thennotcontainsanytext |  |
| Then I expect _the_ 'LOCATOR' to appear exactly 'NUMBER' times | thenappearexactly |  |
| Then I expect _the_ 'LOCATOR' to not appear exactly 'NUMBER' times | thennotappearexactly |  |
| Then I expect _the_ 'LOCATOR' to exist | thenexists |  |
| Then I expect _the_ 'LOCATOR' to not exist | thennotexists |  |
| Then I expect _the_ 'LOCATOR' to be checked | thenchecked |  |
| Then I expect _the_ 'LOCATOR' to not be checked | thennotchecked |  |
| Then I expect _the_ 'LOCATOR' to be selected | thenselected |  |
| Then I expect _the_ 'LOCATOR' to not be selected | thennotselected |  |
| Then I expect _the_ 'LOCATOR' to be enabled | thenenabled |  |
| Then I expect _the_ 'LOCATOR' to be disabled | thendisabled |  |
| Then I expect _the_ 'LOCATOR' to have the class 'CLASS_NAME' | thenclassname |  |
| Then I expect _the_ 'LOCATOR' to not have the class 'CLASS_NAME' | thennotclassname |  |
| Then I expect _the_ 'LOCATOR' to be focused | thenfocused |  |
| Then I expect _the_ 'LOCATOR' to be empty | thenempty |  |
| Then I expect _the_ 'LOCATOR' to not be empty | thennotempty |  |
| Then I expect the value of _the_ 'LOCATOR' to be 'STRING' | thenvalue | Used for getting the value of an input |
| Then I expect the value of _the_ 'LOCATOR' to not be 'STRING' | thennotvalue |  |
| Then I expect the value of _the_ 'LOCATOR' inside _the_ 'LOCATOR' to be 'STRING' | thenelinsideelvalue | This currently only works with XPaths |
| Then I expect the value of _the_ 'LOCATOR' inside _the_ 'LOCATOR' to not be 'STRING' | thennotelinsideelvalue |  |
| Then I expect _the_ 'LOCATOR' has an attribute 'ATTRIBUTE_NAME' with a value of 'VALUE' | thenattribute |  |
| Then fail step and take screenshot | thendie |  |