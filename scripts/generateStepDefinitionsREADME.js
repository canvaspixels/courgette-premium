const fs = require('fs');
const createSnippetsCollection = require('./createSnippetsCollection');

const createStepDefLine = (matcher, code, notes) =>
  [matcher, code, notes || ''];

const padLongestMatcher = (stepDefLns) => {
  const stepDefLines = stepDefLns;
  let longestMatcherLength = 0;
  let longestIndex;

  stepDefLines.forEach((stepDefLine, i) => {
    const len = stepDefLine[0].length;
    if (len > longestMatcherLength && len < 60) {
      longestIndex = i;
      longestMatcherLength = stepDefLine[0].length;
    }
  });

  if (stepDefLines[longestIndex] && stepDefLines[longestIndex][0]) {
    stepDefLines[longestIndex][0] = stepDefLines[longestIndex][0].replace(/ /g, '&nbsp;');
  }

  return stepDefLines;
};

const createStepDefLines = (steps, bookendLeft = '| ', divider = ' | ', bookendRight = ' |', italicReplace = '_$1_') => {
  const newStepDefLines = [];
  steps.forEach((step) => {
    const notMatcher = /not\*/g;
    const newMatcher = step.matcher
      .replace(/\(\?\:(.*?)\)\?/g, (match, p1) => p1.replace(/([^ ]+)/, italicReplace))
      .replace(/\|/g, ' OR ')
      .replace(/\(\?\:(.*)\)/g, '$1')
      .replace(/\((.*)\)/g, '$1');
      // .replace(/\s?/g, '(s)');

    let { notes } = step;
    if (newMatcher.match(notMatcher)) {
      notes = '';
    }
    const stepDefLine = createStepDefLine(newMatcher.replace(notMatcher, 'not'), step.code, notes)
    
    newStepDefLines.push(stepDefLine);
  });

  padLongestMatcher(newStepDefLines);

  return newStepDefLines
    .map((newStepDefLine) => `${bookendLeft}${newStepDefLine.join(divider)}${bookendRight}`);
};

const noPORequiredFilter = (step) => step.pageObjectNotRequired;
const pORequiredFilter = (step) => !step.pageObjectNotRequired;

module.exports = {
  createStepDefLines,
  noPORequiredFilter,
  pORequiredFilter,
}

const givenSteps = createSnippetsCollection.stepsWithSnippetCodes.given;
const whenSteps = createSnippetsCollection.stepsWithSnippetCodes.when;
const thenSteps = createSnippetsCollection.stepsWithSnippetCodes.then;
const givenStepDefLines = [
  '# Available Step Definitions',
  '',
  'Note that the words in italics are optional.',
  '',
  '## Step definitions that _don’t_ require page objects to work',
  '',
  '### Given...',
  '',
  '| Step definition | Snippet Code | Notes |',
  '| --- | --- | --- |',
].concat(createStepDefLines(givenSteps.filter(noPORequiredFilter)));

const whenStepDefLines = [
  '',
  '### When...',
  '',
  '| Step definition | Snippet Code | Notes |',
  '| --- | --- | --- |',
].concat(createStepDefLines(whenSteps.filter(noPORequiredFilter)));

const thenStepDefLines = [
  '',
  '### Then...',
  '',
  '| Step definition | Snippet Code | Notes |',
  '| --- | --- | --- |',
].concat(createStepDefLines(thenSteps.filter(noPORequiredFilter)));

const givenPOStepDefLines = [
  '',
  '',
  '## Step definitions that require page objects to work',
  '',
  '### Given...',
  '',
  '| Step definition | Snippet Code | Notes |',
  '| --- | --- | --- |',
].concat(createStepDefLines(givenSteps.filter(pORequiredFilter)));

const whenPOStepDefLines = [
  '',
  '### When...',
  '',
  '| Step definition | Snippet Code | Notes |',
  '| --- | --- | --- |',
].concat(createStepDefLines(whenSteps.filter(pORequiredFilter)));

const thenPOStepDefLines = [
  '',
  '### Then...',
  '',
  '| Step definition | Snippet Code | Notes |',
  '| --- | --- | --- |',
].concat(createStepDefLines(thenSteps.filter(pORequiredFilter)));

const fileContents = [].concat(
  givenStepDefLines,
  whenStepDefLines,
  thenStepDefLines,
  givenPOStepDefLines,
  whenPOStepDefLines,
  thenPOStepDefLines,
).join('\n');
fs.writeFileSync('./STEP_DEFINITIONS.md', fileContents);
