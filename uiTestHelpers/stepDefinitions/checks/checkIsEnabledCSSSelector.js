module.exports = function checkIsEnabledCSSSelector(cssSelector, enabledOrDisabled) {
  const EC = protractor.ExpectedConditions;

  console.log('            Getting element by css:');
  console.log('              ', cssSelector);
  const el = element(by.css(cssSelector));
  const elIsEnabled = el.isEnabled();
  return browser.wait(EC.presenceOf(el))
    .then(() => enabledOrDisabled === 'enabled' ?
    expect(elIsEnabled).to.eventually.equal(true) :
    expect(elIsEnabled).to.eventually.equal(false));
};
