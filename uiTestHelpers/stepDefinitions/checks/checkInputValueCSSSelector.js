module.exports = function checkInputValueCSSSelector(cssSelector, isNot, expectedVal) {
  const EC = protractor.ExpectedConditions;

  console.log('            Getting element by css:');
  console.log('              ', cssSelector);
  const el = element(by.css(cssSelector));
  const expectedValue = expectedVal === undefined ? '' : expectedVal;
  return browser.wait(EC.presenceOf(el))
    .then(() => {
      const elValuePromise = el.getAttribute('value');
      return isNot ?
        expect(elValuePromise).to.not.eventually.equal(expectedValue) :
        expect(elValuePromise).to.eventually.equal(expectedValue);
    });
};
