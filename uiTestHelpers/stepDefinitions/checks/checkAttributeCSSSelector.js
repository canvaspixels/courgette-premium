module.exports = function checkAttributeCSSSelector(cssSelector, expectedAttribute, expectedValue) {
  const EC = protractor.ExpectedConditions;

  console.log('            Getting element by css:');
  console.log('              ', cssSelector);
  const el = element(by.css(cssSelector));
  return browser.wait(EC.presenceOf(el))
    .then(() => el.getAttribute(expectedAttribute))
    .then((value) => expect(value).to.equal(expectedValue));
};
