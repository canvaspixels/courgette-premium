const helper = require('../helper');

module.exports = function checkElementBackgroundColourCSSSelector(cssSelector, expectedColour) {
  const EC = protractor.ExpectedConditions;

  console.log('            Getting element by css:');
  console.log('              ', cssSelector);
  const el = element(by.css(cssSelector));
  return browser.wait(EC.presenceOf(el))
    .then(() => el.getCssValue('background-color')
      .then((colour) => expect(helper.normalizeColorAcrossBrowser(colour)).to.equal(expectedColour)));
};
