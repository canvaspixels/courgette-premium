const checkColour = require('./checkColour');

module.exports = function checkElementBorderColourCSSSelector(cssSelector, expectedColour) {
  const EC = protractor.ExpectedConditions;

  console.log('            Getting element by css:');
  console.log('              ', cssSelector);
  const el = element(by.css(cssSelector));
  return browser.wait(EC.presenceOf(el))
    .then(() => el.getCssValue(`border-${position || 'bottom'}-color`)
      .then((colour) => expect(helper.normalizeColorAcrossBrowser(colour)).to.equal(expectedColour)));
};
