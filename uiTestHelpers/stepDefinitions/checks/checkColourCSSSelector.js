const helper = require('../helper');

module.exports = function checkColourCSSSelector(cssSelector, expectedColour, property) {
  const EC = protractor.ExpectedConditions;

  console.log('            Getting element by css:');
  console.log('              ', cssSelector);
  const el = element(by.css(cssSelector));
  return browser.wait(EC.presenceOf(el))
    .then(() => {
      return el.getCssValue(property)
        .then((colour) => expect(helper.normalizeColorAcrossBrowser(colour)).to.equal(expectedColour));
    })
};
