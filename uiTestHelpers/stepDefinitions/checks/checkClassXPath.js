module.exports = function checkClassXPath(nth, xpathSelector, doesNotContain, className) {
  const EC = protractor.ExpectedConditions;

  let xpath = xpathSelector;
  if (nth) {
    // remove nd from 2nd for example
    xpath = `(${xpath})[${nth.replace(/\D/g, '')}]`;
  }
  console.log('            Getting element by xpath:');
  console.log('              ', xpath);
  const el = element(by.xpath(xpath));
  return browser.wait(EC.presenceOf(el))
    .then(() => el.getAttribute('class')
      .then((actualClasses) =>
        (doesNotContain ?
          expect(actualClasses).to.not.include(className) :
          expect(actualClasses).to.be.include(className))
      )
    )
};
