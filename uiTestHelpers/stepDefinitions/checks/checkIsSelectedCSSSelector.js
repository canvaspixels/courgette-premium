module.exports = function checkIsSelectedCSSSelector(cssSelector, isNotSelected) {
  const EC = protractor.ExpectedConditions;

  console.log('            Getting element by css:');
  console.log('              ', cssSelector);
  const el = element(by.css(cssSelector));
  return browser.wait(EC.presenceOf(el))
    .then(() => isNotSelected ?
      expect(el.isSelected()).to.eventually.equal(false) :
      expect(el.isSelected()).to.eventually.equal(true));
};
