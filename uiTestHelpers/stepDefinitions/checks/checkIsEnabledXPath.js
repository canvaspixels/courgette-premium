module.exports = function checkIsEnabledXPath(nth, xpathSelector, enabledOrDisabled) {
  const EC = protractor.ExpectedConditions;

  let xpath = xpathSelector;
  if (nth) {
    // remove nd from 2nd for example
    xpath = `(${xpath})[${nth.replace(/\D/g, '')}]`;
  }
  console.log('            Getting element by xpath:');
  console.log('              ', xpath);
  const el = element(by.xpath(xpath));
  return browser.wait(EC.presenceOf(el))
    .then(() => {
      const elIsEnabled = el.isEnabled();

      return enabledOrDisabled === 'enabled' ?
        expect(elIsEnabled).to.eventually.equal(true) :
        expect(elIsEnabled).to.eventually.equal(false);
    });
};
