module.exports = function checkContainsTextXPath(nth, xpathSelector, containsStr, expectedText) {
  const EC = protractor.ExpectedConditions;

  let xpath = xpathSelector;
  if (nth) {
    // remove nd from 2nd for example
    xpath = `(${xpath})[${nth.replace(/\D/g, '')}]`;
  }
  console.log('            Getting element by xpath:');
  console.log('              ', xpath);
  const el = element(by.xpath(xpath));
  return browser.wait(EC.presenceOf(el))
    .then(() => {
      return el.getText().then((text) => {
        if ((typeof containsStr === 'string' && containsStr.indexOf('contain') === 0) || containsStr === true) {
          return expect(text).to.include(expectedText);
        }

        return expect(text, `
Actual: ${text}
Expected: ${expectedText}
          `)
          .to.not.include(expectedText);
      });
    })
};
