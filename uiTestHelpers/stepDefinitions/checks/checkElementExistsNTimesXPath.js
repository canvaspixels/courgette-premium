module.exports = function checkElementExistsNTimesXPath(xpathSelector, doesNotExist, numberOfTimes) {
  const EC = protractor.ExpectedConditions;

  const xpath = xpathSelector;
  console.log('            Getting element by xpath:');
  console.log('              ', xpath);
  const els = elements(by.xpath(xpath));
  const actualCount = browser.wait(protractor.ExpectedConditions.presenceOf(els)).then(() => els.count());
  return doesNotExist ?
    expect(actualCount).to.not.eventually.equal(+numberOfTimes) :
    expect(actualCount).to.eventually.equal(+numberOfTimes);
};
