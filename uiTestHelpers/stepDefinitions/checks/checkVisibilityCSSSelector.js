module.exports = function checkVisibilityCSSSelector(cssSelector, visibleOrHidden) {
  const EC = protractor.ExpectedConditions;

  console.log('            Getting element by css:');
  console.log('              ', cssSelector);
  const el = element(by.css(cssSelector));
  return browser.wait(EC.presenceOf(el))
    .then(() => (visibleOrHidden === 'hidden' ?
      browser.wait(EC.invisibilityOf(el)) :
      browser.wait(EC.visibilityOf(el))))
    .then(() => el);
};
