module.exports = function checkElementExistsCSSSelector(cssSelector, doesNotExist) {
  const EC = protractor.ExpectedConditions;

  console.log('            Getting element by css:');
  console.log('              ', cssSelector);
  const el = element(by.css(cssSelector));
  return doesNotExist ?
    expect(el.isPresent()).to.not.eventually.be.true :
    expect(el.isPresent()).to.eventually.be.true;
};
