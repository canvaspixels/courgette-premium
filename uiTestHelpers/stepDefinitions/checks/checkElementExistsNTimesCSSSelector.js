module.exports = function checkElementExistsNTimesCSSSelector(cssSelector, doesNotExist, numberOfTimes) {
  const EC = protractor.ExpectedConditions;

  console.log('            Getting element by css:');
  console.log('              ', cssSelector);
  const els = elements(by.css(cssSelector));
  const actualCount = browser.wait(protractor.ExpectedConditions.presenceOf(els)).then(() => els.count());

  return doesNotExist ?
    expect(actualCount).to.not.eventually.equal(+numberOfTimes) :
    expect(actualCount).to.eventually.equal(+numberOfTimes);
};
