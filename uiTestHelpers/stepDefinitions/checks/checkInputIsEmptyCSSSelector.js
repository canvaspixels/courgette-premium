module.exports = function checkInputIsEmptyCSSSelector(cssSelector, isNotEmpty) {
  const EC = protractor.ExpectedConditions;

  console.log('            Getting element by css:');
  console.log('              ', cssSelector);
  const el = element(by.css(cssSelector));
  return browser.wait(EC.presenceOf(el))
    .then(() => {
      const elValuePromise = el.getAttribute('value');
      return isNotEmpty ?
        expect(elValuePromise).to.not.eventually.equal('') :
        expect(elValuePromise).to.eventually.equal('');
    });
};
