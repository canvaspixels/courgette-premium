module.exports = function checkAttributeXPath(nth, xpathSelector, expectedAttribute, expectedValue) {
  const EC = protractor.ExpectedConditions;

  let xpath = xpathSelector;
  if (nth) {
    // remove nd from 2nd for example
    xpath = `(${xpath})[${nth.replace(/\D/g, '')}]`;
  }
  console.log('            Getting element by xpath:');
  console.log('              ', xpath);
  const el = element(by.xpath(xpath));
  return browser.wait(EC.presenceOf(el))
    .then(() => el.getAttribute(expectedAttribute))
    .then((value) => expect(value).to.equal(expectedValue));
};
