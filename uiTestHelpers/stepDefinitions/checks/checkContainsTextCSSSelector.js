module.exports = function checkContainsTextCSSSelector(cssSelector, containsStr, expectedText) {
  const EC = protractor.ExpectedConditions;

  console.log('            Getting element by css:');
  console.log('              ', cssSelector);
  const el = element(by.css(cssSelector));
  return browser.wait(EC.presenceOf(el))
    .then(() => {
      return el.getText().then((text) => {
        if ((typeof containsStr === 'string' && containsStr.indexOf('contain') === 0) || containsStr === true) {
          return expect(text).to.include(expectedText);
        }

        return expect(text, `
Actual: ${text}
Expected: ${expectedText}
          `)
          .to.not.include(expectedText);
      });
    });
};
