module.exports = function checkContainsAnyTextCSSSelector(cssSelector, doesNotContain) {
  const EC = protractor.ExpectedConditions;

  console.log('            Getting element by css:');
  console.log('              ', cssSelector);
  const el = element(by.css(cssSelector));
  return browser.wait(EC.presenceOf(el))
    .then(() => doesNotContain ?
        expect(el.getText()).to.eventually.equal('') :
        expect(el.getText()).to.not.eventually.equal(''));
};
