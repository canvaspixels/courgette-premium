module.exports = function checkVisibilityXPath(nth, xpathSelector, visibleOrHidden) {
  const EC = protractor.ExpectedConditions;

  let xpath = xpathSelector;
  if (nth) {
    // remove nd from 2nd for example
    xpath = `(${xpath})[${nth.replace(/\D/g, '')}]`;
  }
  console.log('            Getting element by xpath:');
  console.log('              ', xpath);
  const el = element(by.xpath(xpath));
  return browser.wait(EC.presenceOf(el))
    .then(() => (visibleOrHidden === 'hidden' ?
      browser.wait(EC.invisibilityOf(el)) :
      browser.wait(EC.visibilityOf(el))))
    .then(() => el);
};
