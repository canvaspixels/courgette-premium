module.exports = function checkClassCSSSelector(cssSelector, doesNotContain, className) {
  const EC = protractor.ExpectedConditions;

  console.log('            Getting element by css:');
  console.log('              ', cssSelector);
  const el = element(by.css(cssSelector));
  return browser.wait(EC.presenceOf(el))
    .then(() => {
      const classNameToBeChecked = el.getAttribute('class');

      return classNameToBeChecked
        .then((actualClasses) =>
          (doesNotContain ?
            expect(actualClasses).to.not.include(className) :
            expect(actualClasses).to.be.include(className)));
    });
};
