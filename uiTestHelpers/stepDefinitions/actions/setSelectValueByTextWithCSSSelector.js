const setOptionByText = require('./helpers/setOptionByText');

module.exports = function setSelectValueByTextWithCSSSelector(cssSelector, optionText) {
  const EC = protractor.ExpectedConditions;

  console.log('            Getting element by css:');
  console.log('              ', cssSelector);
  const el = element(by.css(cssSelector));
  return browser.wait(EC.presenceOf(el))
    .then(() => setOptionByText(el, optionText));
};
