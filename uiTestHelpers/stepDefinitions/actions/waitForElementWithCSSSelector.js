module.exports = function waitForElementWithCSSSelector(cssSelector, doesNotExist) {
  const EC = protractor.ExpectedConditions;

  console.log('            Getting element by css:');
  console.log('              ', cssSelector);
  const el = element(by.css(cssSelector));
  return doesNotExist ? browser.wait(EC.stalenessOf(el)) : browser.wait(EC.presenceOf(el));
};
