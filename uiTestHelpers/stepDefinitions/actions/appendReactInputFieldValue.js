// in some circumstances using send keys to set text on an input doesn't
// fire the change event correcly so for example your redux store might not be updated
// this function is particularly useful if you want to send a lot of text to the
// browser all at once as sendKeys is particularly slow
const appendToReactField = require('./helpers/appendToReactField');

module.exports = function appendReactInputFieldValue(text, locatorKey) {
  return this.getCurrentPage()
    .getElementWhenInDOM(locatorKey)
    .then((element) => browser.executeScript(appendToReactField, element, text));
};
