module.exports = (el, optionText) => {
  let desiredOption;

  el.click();

  return el.$$('option')
    .then((options) => Promise.all(options.map((option) => option.getText().then((text) => {
      if (optionText === text) {
        desiredOption = option;
      }
    }))))
    .then(() => {
      if (desiredOption) {
        return desiredOption.click();
      }
      return Promise.reject(new Error('no option found'));
    });
}
