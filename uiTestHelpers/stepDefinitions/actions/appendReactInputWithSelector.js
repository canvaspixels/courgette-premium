// in some circumstances using send keys to set text on an input doesn't
// fire the change event correcly so for example your redux store might not be updated
// this function is particularly useful if you want to send a lot of text to the
// browser all at once as sendKeys is particularly slow
const appendToReactField = require('./helpers/appendToReactField');

module.exports = function appendReactInputWithSelector(value, cssSelector) {
  const EC = protractor.ExpectedConditions;

  console.log('            Getting element by css:');
  console.log('              ', cssSelector);
  const el = element(by.css(cssSelector));
  return browser.wait(EC.presenceOf(el))
    .then(() => browser.executeScript(appendToReactField, el, text));
}
