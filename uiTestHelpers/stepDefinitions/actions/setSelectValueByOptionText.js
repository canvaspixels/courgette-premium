const setOptionByText = require('./helpers/setOptionByText');

module.exports = function setSelectValueByOptionText(locatorKey, itemText) {
  return this.getCurrentPage()
    .getElementWhenInDOM(locatorKey)
    .then((element) => setOptionByText(element, itemText));
};
