import React from 'react';
import { NavLink } from 'react-router-dom';
import './HomePage.scss';
import CommonTemplate from './CommonTemplate';
import { ReactComponent as ScreenshotsIcon } from './images/screenshots-reports.svg'
import { ReactComponent as DevicesIcon } from './images/devices.svg'
import { ReactComponent as PeopleIcon } from './images/people.svg'

const APIPage = () => (
  <CommonTemplate className="home">
    <div>
      <section className="hero">
        <h1 className="strap">
          Beautifully simple UI Testing <br />
          For all the team
        </h1>
        <NavLink to="/getting-started" className="cta">Get started</NavLink>
      </section>
      <section className="usps">
        <div className="usps__inner">
          <div className="usp">
            <div className="usp__desc">
              <ScreenshotsIcon className="usp__icon-image icon-screenshots" />
              <h2 className="usp__title">Screenshots and Reporting</h2>
              <p>Screenshots are taken on test failure to help debugging or on demand. A gallery is included for visual checks before go-live. HTML and CLI reports for step definition usage, passes / failures</p>
            </div>
          </div>
          <div className="usp">
            <div className="usp__desc">
              <DevicesIcon className="usp__icon-image icon-devices" />
              <h2 className="usp__title">Multiple browsers &amp; platforms</h2>
              <p>Runs on Windows, Mac and Linux, Android &amp; iOS (via appium and WDIO)</p>
            </div>
          </div>
          <div className="usp">
            <div className="usp__desc">
              <PeopleIcon className="usp__icon-image icon-people" />
              <h2 className="usp__title">Made for teams&hellip; not just developers</h2>
              <p>Improve communication and confidence by allowing non-technical team members to see what is under test</p>
            </div>
          </div>
        </div>
      </section>


      <section className="hero">
        <h1 className="strap">
          Bringing sanity to <br /> selenium-based UI test automation
        </h1>
        <section className="usps usps2">
          <div className="usps__inner">
            <div className="usp">
              <div className="usp__desc">
                <h2 className="usp__title">Gherkin Syntax</h2>
                <p>Automate Given When Then scenarios without writing any code</p>
              </div>
            </div>
            <div className="usp">
              <div className="usp__desc">
                <h2 className="usp__title">Page / Component Objects</h2>
                <p>YAML-based Page and Component Objects aid in scaling up your test suite</p>
              </div>
            </div>
            <div className="usp">
              <div className="usp__desc">
                <h2 className="usp__title">Selenium Grid</h2>
                <p>Easy integration with Sauce Labs &amp; Browser Stack</p>
              </div>
            </div>
            <div className="usp">
              <div className="usp__desc">
                <h2 className="usp__title">Snippets / Live Templates</h2>
                <p>Installed straight to your favourite IDE* to enable quick and accurately writing of scenarios</p>
                <small>* supports Intellij, Webstorm, Sublime Text, Textmate, Atom, VSCode</small>
              </div>
            </div>
          </div>
        </section>
      </section>

      <section className="container hp-signup-form" id="newsletter-subscribe">
        <div className="container__inner">
          <div id="mc_embed_signup">
            <form action="https://canvaspixels.us20.list-manage.com/subscribe/post?u=6630e9c5ae474be3677db0eb8&amp;id=bb6c5660a8" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" className="validate" target="_blank" noValidate>
                <div id="mc_embed_signup_scroll">
              <h2>Subscribe to the newsletter</h2>
              <p>We promise we won’t spam you!</p>
              <div className="mc-field-group">
                <label htmlFor="mce-EMAIL">Email Address</label>
                <input type="email" name="EMAIL" className="required email" id="mce-EMAIL"/>
                <div style={{position: 'absolute', left: -5000}} aria-hidden="true"><input type="text" name="b_6630e9c5ae474be3677db0eb8_bb6c5660a8" tabIndex="-1"/></div>
                <div className="clear"><button type="submit" name="subscribe" id="mc-embedded-subscribe" className="button cta">Subscribe</button></div>
              </div>
              <div id="mce-responses" className="clear">
                <div className="response" id="mce-error-response" style={{display:'none'}}></div>
                <div className="response" id="mce-success-response" style={{display:'none'}}></div>
              </div>
              </div>
            </form>
          </div>
        </div>
      </section>



      <section className="testimonials">
        <div className="testimonial">
          <blockquote>
            <q>it comes with batteries included to some extent so you can get up and running quickly</q>
            <p>Dan North (Coined BDD)</p>
          </blockquote>
        </div>
        <div className="testimonial">
          <blockquote>
            <q>I instantly get it, it’s like a DSL that everyone can read and understand</q>
            <p>Pendar Ghalamchi (Developer at Capital One)</p>
          </blockquote>
        </div>
      </section>
      <section className="customers">
        <h2>Companies using Courgette</h2>
        <ul className="customers__list">
          <li className="customer">
            Capital One
          </li>
          <li className="customer">
            Linklaters
          </li>
          <li className="customer">
            Amazon
          </li>
          <li className="customer">
            lastminute.com
          </li>
        </ul>
      </section>
      <section className="hero">
        <NavLink to="/getting-started" className="cta">Get started</NavLink>
      </section>
    </div>
  </CommonTemplate>
);

export default APIPage;
