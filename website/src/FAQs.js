import React, { Component } from 'react';
import CommonTemplate from './CommonTemplate';
import './FAQs.scss';

class FAQs extends Component {

  qs = [
    {
      q: 'How do you install and run courgette behind a corporate proxy?',
      a: (
 <div dangerouslySetInnerHTML={{__html: `
        <p>Add the following to the capabilities section of your courgette-conf.js file:</p>
<pre>
  proxy: {
    proxyType: 'manual',
    httpProxy: 'hostname.com:1234'
  }
</pre>
<p>This uses the <a href="https://www.npmjs.com/package/protractor-cucumber-framework">cucumber protractor framework</a>, so be sure to check that out for more information</p>
`}} />
      )
    },
    {
      q: 'How do you run your tests in parallel?',
      a: <>By default feature files are run in parallel unless you have the linearise environment variable set or you are running a specific tag or tags. Therefore when you run all your tests together (npm run ct), make sure that the feature files use different test accounts/users if you are working with accounts and make sure they don’t conflict with each other.</>
    },
    {
      q: 'How do you test an application behind a login? Do you need to complete the login form before each scenario?',
      a: (
 <div dangerouslySetInnerHTML={{__html: `
        <p>You’re probably going to want to test that your login form works (error states etc), however if you have 500 tests, you don’t want your script to have to login 500 times as this will significantly increase the test-run time. A strategy could be to set up a test endpoint (only available on dev environments) that automatically sets your session cookie / returns you a JWT (depending on your architecture). You can then use protractor’s addCookie method e.g.:</p>
<pre>
  browser.manage().addCookie('foo', 'bar', '/', 'YOURDOMAIN.com');
</pre>
<p>and add it to a custom step definition once the login page has loaded.</p>
`}} />
      )
    },
    {
      q: 'How do you test an application running on localhost over HTTPS when the add exception page keeps showing?',
      a: 'acceptInsecureCerts is set to true by default in the capabilities in courgette-conf.js as of a previous release, and for Firefox the useragent has been overridden. This should be enough to get things going.'
    },
    // {
    //   q: 'The step is unknown',
    //   a: 'Could be that your courgette version is not the latest'
    // },
  ]

  render() {
    return (
      <CommonTemplate className="">
        <h1>“Frequently” “asked” questions</h1>
        <p className="faqs__intro">None of these questions were <em>frequently</em> asked, nor were they actually ever <em>asked</em> to any of the Courgette team, but they're problems that a lot of people come across when automating UI tests. They're here nonetheless to help you people.</p>

        <div className="faqs">
        { this.qs.map((q, i) => (
          <div className="faq" key={i}>
            <div className="faq__q">
              {/*<a href="/faqs" onClick={() => { this.setState({  }) }}>{q.q}</a>*/}
              <h2>{q.q}</h2>
            </div>
            <div className="faq__a">
              {q.a}
            </div>
          </div>
        ))}
        </div>

      </CommonTemplate>
    );
  }
}

export default FAQs;
