import Cookies from 'js-cookie'

function addMailChimpScript(cb) {
  var s = document.createElement( 'script' );
  s.setAttribute( 'src', '//downloads.mailchimp.com/js/signup-forms/popup/unique-methods/embed.js' );
  s.setAttribute( 'data-dojo-config', 'usePlainJson: true, isDebug: false' );
  document.body.appendChild( s );
  s.onload=cb;
}

function showSubscribeSetCookie() {
  window.dojoRequire(["mojo/signup-forms/Loader"], function(L) {
    L.start({"baseUrl":"mc.us20.list-manage.com","uuid":"6630e9c5ae474be3677db0eb8","lid":"bb6c5660a8","uniqueMethods":true})
      function showBg() {
          const modalBgs = document.querySelectorAll('.mc-modal-bg')
          modalBgs.forEach((modalBg) => {
            if (modalBg && modalBg.style.display === 'block' && modalBg.style.visibility !== 'visible') {
              modalBg.style.visibility = 'visible';
              clearInterval(interval)
            }
          })
      }
      const interval = setInterval(showBg, 100)
  })
}

function subscribe() {
  Cookies.remove('MCPopupClosed')
  Cookies.remove('MCPopupSubscribed')
  console.log(window.document.cookie);
  showSubscribeSetCookie();
}

export default {
  showSubscribeSetCookie,
  addMailChimpScript,
  subscribe,
}