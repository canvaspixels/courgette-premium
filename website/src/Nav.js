import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
// import { withRouter } from 'react-router-dom';

import './nav.scss';

class Nav extends Component {
  state = {}
  // componentDidMount() {
  //   this.props.history.listen((location, action) => {
  //       // location is an object like window.location
  //       console.log(action, location.pathname, location.state)
  //   });
  // }

  render() {
    const { showMenu } = this.state;
    return (
      <div className={`nav ${showMenu && 'show'}`}>
        <header>
          <div className="nav__header-left">
            <button type="button" onClick={() => this.setState({showMenu: true})} className="burger">Menu</button>
            <p className="logo">
              <a href="/">Courgette <span className="logo__sub">Testing</span></a>
            </p>
          </div>
          <ul>
            <li><NavLink to="/getting-started" activeClassName="selected">Getting Started</NavLink></li>
            <li><NavLink to="/guide" activeClassName="selected">Guide</NavLink></li>
            <li><NavLink to="/step-definitions" activeClassName="selected">Steps</NavLink></li>
            <li><NavLink to="/mobile-step-definitions" activeClassName="selected">Mobile Steps</NavLink></li>
            <li><NavLink to="/api" activeClassName="selected">API</NavLink></li>
            <li><NavLink to="/bdd" activeClassName="selected">BDD</NavLink></li>
            <li><a href="/blog/">Blog</a></li>
            <li><a href="https://github.com/canvaspixels/courgette" target="_blank" rel="noopener noreferrer">Github</a></li>
          </ul>
        </header>
        <div className="overlay" onClick={() => this.setState({showMenu: false})}>
          <div className="overlay-inner" />
        </div>
        <div className="main-nav">
          <button type="button" className="close-btn" onClick={() => this.setState({showMenu: false})}>Close</button>
          <div className="main-nav__inner">
            <ul>
              <li><NavLink to="/" activeClassName="selected" exact>Overview</NavLink></li>
              <li><NavLink to="/getting-started" activeClassName="selected">Getting Started</NavLink></li>
              <li><NavLink to="/guide" activeClassName="selected">Guide</NavLink></li>
              <li><NavLink to="/step-definitions" activeClassName="selected">Steps</NavLink></li>
              <li><NavLink to="/mobile-step-definitions" activeClassName="selected">Mobile Steps</NavLink></li>
              <li><NavLink to="/api" activeClassName="selected">API</NavLink></li>
              <li><NavLink to="/faqs" activeClassName="selected">FAQs</NavLink></li>
              <li><NavLink to="/bdd" activeClassName="selected">BDD - User Stories</NavLink></li>
              <li><a href="/blog/">Blog</a></li>
              <li><a href="https://github.com/canvaspixels/courgette" target="_blank" rel="noopener noreferrer">Github</a></li>
            </ul>
          </div>
        </div>
      </div>
    )
  }
}

export default Nav;
