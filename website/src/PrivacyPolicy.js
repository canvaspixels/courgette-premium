import React, { Component } from 'react';
import CommonTemplate from './CommonTemplate';

class PrivacyPolicy extends Component {

  render() {
//   <li>First name and last name</li>
//   <li>Phone number</li>
//   <li>Address and Postcode</li>
    return (
      <CommonTemplate className="">
        <div dangerouslySetInnerHTML={{__html: `
        <h1>Privacy Policy</h1>

<p>Effective date: 25 September, 2019</p>

<p>Canvas Pixels Ltd ("us", "we", or "our") operates the https://courgette-testing.com website (the "service").</p>
<p>This page informs of our policies regarding the use, collection, and disclosure of personal data when you use this service and the choices you have associated with that data.</p>
<p>We use your data to improve this Service and by using it you agree to the collection and use of information in accordance with this policy.</p>

<h2>Information collection and use</h2>
<p>We collect several different types of information for various purposes to improve our Service to you.</p>

<h3>Types of data collected</h3>
<h4>Personal data</h4>
<p>While using our Service, we may ask you to provide us with certain personally identifiable information that can be used to contact or identify you ("Personal Data"). Personally identifiable information may include, but is not limited to:</p>

<ul>
  <li>Cookies and Usage Data</li>
  <li>Email address</li>
</ul>

<h4>Usage data</h4>
<p>We may also collect information how this Service is accessed and used ("Usage Data"). This Usage Data may include information such as your computer’s Internet Protocol (or IP) address, browser type, browser version, the pages of our Service that you visit, the time and date of your visit, the time spent on those pages, unique device identifiers and other diagnostic data.</p>

<h4>Tracking &amp; Cookie data</h4>
<p>We use cookies and similar tracking technologies to track your activity on our Service and hold certain information.</p>
<p>Cookies are files with a small amount of data which may include an anonymous unique identifier. Cookies are sent to your browser from a website and stored on your device or computer. Tracking technologies also used are beacons, tags, and scripts to collect and track information and to improve and analyse this Service.</p>
<p>You can instruct your browser to refuse all cookies or to indicate when a cookie is being sent.</p>
<p>Examples of cookies we use:</p>
<ul>
    <li><strong>Preference Cookies.</strong> We use Preference Cookies to remember your preferences and various settings.</li>
    <li><strong>Security Cookies.</strong> We use Security Cookies for security purposes.</li>
</ul>

<h2>Use of Data</h2>
<p>Canvas Pixels Ltd uses the collected data for various purposes:</p>
<ul>
    <li>To provide and maintain the Service</li>
    <li>To provide analysis or valuable information so that we can improve the Service</li>
    <li>To monitor the usage of the Service</li>
    <li>To detect, prevent and address technical issues</li>
</ul>

<h2>Transfer Of Data</h2>
<p>Your information, including Personal Data, may be transferred to and maintained on computers located outside of your state, province, country or other governmental jurisdiction where the data protection laws may differ than those from your jurisdiction.</p>
<p>If you are located outside United Kingdom and choose to provide information to us, please note that we transfer the data, including Personal Data, to United Kingdom and process it there.</p>
<p>Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.</p>
<p>Canvas Pixels Ltd will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this Privacy Policy and no transfer of your Personal Data will take place to an organization or a country unless there are adequate controls in place including the security of your data and other personal information.</p>

<h2>Disclosure Of Data</h2>

<h3>Legal Requirements</h3>
<p>Canvas Pixels Ltd may disclose your Personal Data in the good faith belief that such action is necessary to:</p>
<ul>
    <li>To comply with a legal obligation</li>
    <li>To protect and defend the rights or property of Canvas Pixels Ltd</li>
    <li>To prevent or investigate possible wrongdoing in connection with the Service</li>
    <li>To protect the personal safety of users of this Service or the public</li>
    <li>To protect against legal liability</li>
</ul>

<h2>Security Of Data</h2>
<p>The security of your data is important to us, but remember that no method of transmission over the Internet, or method of electronic storage is 100% secure. While we strive to use commercially acceptable means to protect your Personal Data, we cannot guarantee its absolute security.</p>

<h2>Service Providers</h2>
<p>We may employ third party companies and individuals to facilitate our Service ("Service Providers"), to provide the Service on our behalf, to perform Service-related services or to assist us in analyzing how our Service is used.</p>
<p>These third parties have access to your Personal Data only to perform these tasks on our behalf and are obligated not to disclose or use it for any other purpose.</p>

<h3>Analytics</h3>
<p>We use the following third-party Service Provider to monitor and analyse the use of this Service.</p>
        <h4>Google Analytics</h4>
        <p>Google Analytics is a web analytics service offered by Google that tracks and reports website traffic. Google uses the data collected to track and monitor the use of this Service. This data is shared with other Google services. Google may use the collected data to contextualise and personalise the ads of its own advertising network.</p>
        <p>You can opt-out of having made your activity on the Service available to Google Analytics by installing the Google Analytics opt-out browser add-on. The add-on prevents the Google Analytics JavaScript (ga.js, analytics.js, and dc.js) from sharing information with Google Analytics about visits activity.</p>                <p>For more information on the privacy practices of Google, please visit the Google Privacy & Terms web page: <a href="https://policies.google.com/privacy?hl=en">https://policies.google.com/privacy?hl=en</a></p>

<h2>Links to other sites</h2>
<p>Our Service may contain links to other sites that are not operated by us. If you click on a third party link, you will be directed to that third party’s site. We strongly advise you to review the Privacy Policy of every site you visit.</p>
<p>We have no control over and assume no responsibility for the content, privacy policies or practices of any third party sites or services.</p>

<h2>Children’s Privacy</h2>
<p>Our Service does not address anyone under the age of 18 ("Children").</p>
<p>We do not knowingly collect personally identifiable information from anyone under the age of 18. If you are a parent or guardian and you are aware that your Children have provided us with Personal Data, please contact us. If we become aware that we have collected Personal Data from Children without verification of parental consent, we will take steps to remove that information from our servers.</p>

<h2>Changes to this Privacy Policy</h2>
<p>We may update our Privacy Policy from time to time. We will notify you of any changes by posting the new Privacy Policy on this page.</p>
<p>We will let you know via email and/or a prominent notice on our Service, prior to the change becoming effective and update the "effective date" at the top of this Privacy Policy.</p>
<p>You are advised to review this Privacy Policy periodically for any changes. Changes to this Privacy Policy are effective when they are posted on this page.</p>

<h2>Contact Us</h2>
<p>If you have any questions about this Privacy Policy, please contact us by raising an issue on the <a href="https://github.com/canvaspixels/courgette/issues">courgette github page</a></p>
`}} />
      </CommonTemplate>
    );
  }
}

export default PrivacyPolicy;
