import React, { useState } from 'react'

import Cookies from 'js-cookie'

export default function GDPR() {
  const [isPolicyAgreed, setPolicy] = useState(!!Cookies.get('privacyPolicyAgreed'));
  return (
    <div className="gdpr-banner-wrapper" style={{display:isPolicyAgreed ? 'none' : 'block'}}>
      <div className="gdpr-banner">
        <h2 className="gdpr-banner-title">We've changed our Privacy and Cookie Policy</h2>

        <p className="gdpr-banner-content">Our Privacy and Cookie Policy has changed. By continuing to use this site without changing your cookie settings, we assume that you're comfortable with this.</p>

        <ul className="gdpr-banner-options">
          <li>
            <button type="button" className="gdpr-ok-button" onClick={() => {
              Cookies.set('privacyPolicyAgreed', 'true')
              setPolicy(true)
            }}>OK, I Understand</button>
          </li>
          <li>
            <a href="/privacy-policy">Read our Privacy Policy</a>
          </li>
        </ul>
      </div>
    </div>
  )
}

