/* eslint no-useless-escape: 0 */
import React, { Component } from 'react';
import CommonTemplate from './CommonTemplate';
import { animateScroll } from 'react-scroll';
import './Guide.scss';

class Guide extends Component {
  state={}

  componentDidMount() {
    const el = document.querySelector(`[name="${window.location.hash.replace(/^#/, '')}"]`)
    if (el) {
      animateScroll.scrollTo(el.getBoundingClientRect().top + window.scrollY - 150);
    }
  }

  scrollTo = (e) => {
    const el = document.querySelector(`[name="${e.target.getAttribute('href').replace(/^#/, '')}"]`)

    if (el) {
      animateScroll.scrollTo(el.getBoundingClientRect().top + window.scrollY - 150);
    }
    this.setState({showMenu: false})
    // e.preventDefault()
  }

  render() {
    const { showMenu } = this.state;

    return (
      <CommonTemplate className="">
        <div className="guide">
          <div className="burger-wrap"><button type="button" onClick={() => this.setState({showMenu: true})} className="burger">Menu</button> Quick menu</div>
          <div className={`sub-nav ${showMenu && 'show'}`}>
            <div className="overlay" onClick={() => this.setState({showMenu: false})}>
              <div className="overlay-inner" />
            </div>
            <div className="main-nav">
              <button type="button" className="close-btn" onClick={() => this.setState({showMenu: false})}>Close</button>
              <div className="main-nav__inner">
                <h1>Guide</h1>
                <ul>
                  <li><a onClick={this.scrollTo} href="#user-content-feature-file-by-example">Feature file by example</a></li>
                  <li><a onClick={this.scrollTo} href="#user-content-running-just-one-feature-or-one-scenario">Running just one feature or one scenario</a></li>
                  <li><a onClick={this.scrollTo} href="#user-content-viewing-your-test-run">Viewing your test run</a></li>
                  <li><a onClick={this.scrollTo} href="#user-content-courgette-confjs-file">courgette-conf.js file</a></li>
                  <li><a onClick={this.scrollTo} href="#user-content-page-files-and-component-files">.page files and .component files</a></li>
                  <li><a onClick={this.scrollTo} href="#user-content-page-file-example">.page file example</a></li>
                  <li><a onClick={this.scrollTo} href="#user-content-component-file-example">.component file example</a></li>
                  <li><a onClick={this.scrollTo} href="#user-content-parallelisation">Parallelisation</a></li>
                  <li><a onClick={this.scrollTo} href="#user-content-snippets--live-templates">Snippets / Live templates</a></li>
                  <li><a onClick={this.scrollTo} href="#user-content-combining-steps">Combining steps</a></li>
                  <li><a onClick={this.scrollTo} href="#user-content-tidying-up-unused-step-definitions">Tidying up unused step definitions</a></li>
                  <li><a onClick={this.scrollTo} href="#user-content-tidying-page-objects">Tidying page objects</a></li>
                </ul>
              </div>
            </div>
          </div>

          <div dangerouslySetInnerHTML={
            {__html: `

    <div>
      <h1>Guide</h1>
      <p>Make sure you’ve read the <a href="/getting-started">Getting Started</a> page. The best way to learn courgette is by example. The installation creates a <code>uiTests</code> folder with some sample Scenarios. Have a look in the features folder, tweak some values, and rerun the tests:</p>
      <div class="pre-wrap"><button type="button" class="copy-code-btn">Copy</button><pre class="js-to-copy">npx ct</pre></div>
      <p>To make things easier to read, make sure that you have the Gherkin and YAML syntax packages for your IDE. YAML usually comes with most IDEs. You’ll want to associate <code>.page</code> and <code>.component</code> extensions with YAML and <code>.steps</code> and <code>.feature</code> with Gherkin.</p>
      <p>See the comments in the sample <code>.feature</code> file for more information.</p>



                        <h2><a name="user-content-feature-file-by-example" class="anchor" aria-hidden="true" href="#feature-file-by-example"></a>Feature file by example</h2>
                        <div class="highlight highlight-text-gherkin-feature">
                            <div class="pre-wrap">
  <button type="button" class="copy-code-btn">Copy</button><pre class="js-to-copy"><span class="pl-k">@google-home</span>
<span class="pl-k">Feature</span>:<span class="pl-s"> Test feature</span>

  <span class="pl-k">@google-home-feeling-lucky</span>
  <span class="pl-k">Scenario</span>:<span class="pl-s"> I am testing this out</span>
    <span class="pl-k">Given </span>I am on the <span class="pl-s">'Google Home'</span> page
    <span class="pl-k">When </span>I click <span class="pl-s">'I’m Feeling Lucky'</span>
    <span class="pl-k">Then </span>I expect the url to contain <span class="pl-s">'google.com'</span></pre>
    </div>
                        </div>
                        <p>All you need to be able to run the scenario above is a page object that looks like this inside a kebab-case yaml file e.g. <code>google-home.page</code>, placed in the <code>uiTests/pages</code> folder:</p>
                        <div class="highlight highlight-source-yaml">
                            <div class="pre-wrap">
  <button type="button" class="copy-code-btn">Copy</button><pre class="js-to-copy"><span class="pl-ent">path</span>: <span class="pl-s">https://www.google.com/</span>

<span class="pl-ent">selectors</span>:
  <span class="pl-ent">I’m Feeling Lucky</span>: <span class="pl-s">[value="I'm Feeling Lucky"]</span></pre>
  </div>
                        </div>
                        <p>...or if you want the boilerplate js code and need more flexibility place the following in <code>google-home.js</code> in the same folder (again kebab-case is important):</p>
                        <div class="highlight highlight-source-js">
                            <div class="pre-wrap">
  <button type="button" class="copy-code-btn">Copy</button><pre class="js-to-copy"><span class="pl-k">const</span> <span class="pl-c1">createPage</span> <span class="pl-k">=</span> <span class="pl-c1">require</span>(<span class="pl-s"><span class="pl-pds">'</span>courgette/uiTestHelpers/createPage<span class="pl-pds">'</span></span>);
<span class="pl-k">const</span> <span class="pl-c1">fileName</span> <span class="pl-k">=</span> <span class="pl-smi">createPage</span>.<span class="pl-en">getFileName</span>(<span class="pl-c1">__filename</span>);

<span class="pl-c1">module</span>.<span class="pl-en">exports</span> <span class="pl-k">=</span> (<span class="pl-smi">world</span>) <span class="pl-k">=&gt;</span> {
  <span class="pl-k">const</span> <span class="pl-c1">pagePath</span> <span class="pl-k">=</span> <span class="pl-s"><span class="pl-pds">'</span>https://www.google.com/<span class="pl-pds">'</span></span>;
  <span class="pl-k">const</span> <span class="pl-c1">locators</span> <span class="pl-k">=</span> {
    <span class="pl-s"><span class="pl-pds">'</span>I’m Feeling Lucky<span class="pl-pds">'</span></span><span class="pl-k">:</span> <span class="pl-smi">by</span>.<span class="pl-en">css</span>(<span class="pl-s"><span class="pl-pds">'</span>[value="I<span class="pl-cce">\'</span>m Feeling Lucky"]<span class="pl-pds">'</span></span>),
  };

  <span class="pl-k">return</span> <span class="pl-en">createPage</span>(fileName, world, pagePath, locators);
};</pre>
</div>
                        </div>
                        <p>Note that a <code>.page</code> file will take precedence over a <code>.js</code> page object file.</p>
                        <p>You don't need to write any page object methods, nor step definitions. How easy is that!!?</p>
                        <p>The indentation in YAML (the .page / .component files) is important. The keys such as path, components, selectors, xpaths, deepSelectors need to always be on the far left as per the examples on this page.</p>
                        <p>It's important that the page object name is kebab-case and lowercase. E.g. <code>about-us.js</code> or <code>about-something-else.js</code> or <code>google-home.js</code> as in the sample. <code>Given I am on the 'Google Home' page</code> sets the current page object and <code>Google Home</code> gets translated behind the scenes to <code>google-home.js</code> so make sure <code>Google Home</code> has the space in it.</p>
                        <p>It’s advisable when writing your features to add a tag at the top of the Feature file and a tag to the beginning of each Scenario. A tag starts with a @. As a convention you can prefix each Scenario tag with whatever you've used at the top of the file (in this case @google-home). Try and keep them unique for your ease of use.</p>
                        <p>Note you can add more than one tag to each scenario and you could tag them when a hook tag that you can hook into Before or After each scenario. <a href="https://github.com/cucumber/cucumber-js/blob/master/docs/support_files/hooks.md">Read more about hooks</a> just add hooks to the existing ones in your courgette-conf.js file.</p>
                        <div class="highlight highlight-text-gherkin-feature">
                            <div class="pre-wrap">
  <button type="button" class="copy-code-btn">Copy</button><pre class="js-to-copy"><span class="pl-k">@google-home</span>
<span class="pl-k">Feature</span>:<span class="pl-s"> Test feature</span>

  <span class="pl-k">@google-home-feeling-lucky</span>
  <span class="pl-k">Scenario</span>:<span class="pl-s"> ...</span>
    <span class="pl-k">Given </span>...
    <span class="pl-k">When </span>...
    <span class="pl-k">Then </span>...

  <span class="pl-k">@google-home-another-thing</span> <span class="pl-k">@some-special-hook-before-each-run</span>
  <span class="pl-k">Scenario</span>:<span class="pl-s"> ...</span>
    <span class="pl-k">Given </span>...
    <span class="pl-k">When </span>...
    <span class="pl-k">Then </span>...

  <span class="pl-k">@google-home-yet-another-thing</span> <span class="pl-k">@some-special-hook-before-each-run</span>
  <span class="pl-k">Scenario</span>:<span class="pl-s"> ...</span>
    <span class="pl-k">Given </span>...
    <span class="pl-k">When </span>...
    <span class="pl-k">Then </span>...</pre>
    </div>
                        </div>
                        <h2><a name="user-content-running-just-one-feature-or-one-scenario" class="anchor" aria-hidden="true" href="#running-just-one-feature-or-one-scenario"></a>Running just one feature or one scenario</h2>
                        <p>Continuing on from the examples above...</p>
                        <p>To run just one feature (assuming the @tag is at the top of the file):</p>
                        <div class="highlight highlight-text-shell-session">
                            <div class="pre-wrap">
  <button type="button" class="copy-code-btn">Copy</button><pre class="js-to-copy"><span class="pl-c1">npx ct @google-home</span></pre>
  </div>
                        </div>
                        <p>To run just one scenario (assuming you’ve added the @tag above your scenario):</p>
                        <div class="highlight highlight-text-shell-session">
                            <div class="pre-wrap">
  <button type="button" class="copy-code-btn">Copy</button><pre class="js-to-copy"><span class="pl-c1">npx ct @google-home-another-thing</span></pre>
  </div>
                        </div>
                        <p>To run a couple (comma separate):</p>
                        <div class="highlight highlight-text-shell-session">
                            <div class="pre-wrap">
  <button type="button" class="copy-code-btn">Copy</button><pre class="js-to-copy"><span class="pl-c1">npx ct @google-home-feeling-lucky,@google-home-another-thing</span></pre>
  </div>
                        </div>
                        <h2><a name="user-content-viewing-your-test-run" class="anchor" aria-hidden="true" href="#viewing-your-test-run"></a>Viewing your test run</h2>
                        <p>If you get an error, you'll see a screenshot for each step error inside the <code>uiTestResult</code> folder and a link to each one from the console output in your terminal. However, if you want to view the browser running each step, you can put DH=1 (for disable headless, or COURGETTE_HEADLESS=false if you prefer) before the courgette command like this:</p>
                        <div class="highlight highlight-text-shell-session">
                            <div class="pre-wrap">
  <button type="button" class="copy-code-btn">Copy</button><pre class="js-to-copy"><span class="pl-c1">DH=1 npx ct</span></pre>
  </div>
                        </div>
                        <p>That'll launch Firefox by default and you'll be able to see your tests run.</p>
                        <h2><a name="user-content-courgette-confjs-file" class="anchor" aria-hidden="true" href="#courgette-confjs-file"></a>courgette-conf.js file</h2>
                        <p>The conf file allows you to specify the following:</p>
                        <ul>
                            <li>folder locations for the following
                                <ul>
                                    <li>test results</li>
                                    <li>page objects</li>
                                    <li>component objects</li>
                                    <li>feature files</li>
                                    <li>hooks</li>
                                </ul>
                            </li>
                            <li>step timeoutInSeconds - how long a step will wait to complete before it times out</li>
                            <li>baseUrl - the hostname will prefix the paths you set in your page objects</li>
                            <li>capabilities for different browsers, Firefox is the default. You can point to services like browserstack or saucelabs to test a matrix of platforms and browsers</li>
                        </ul>
                        <p>To point to a different configuration file:</p>
                        <div class="highlight highlight-text-shell-session">
                            <div class="pre-wrap">
  <button type="button" class="copy-code-btn">Copy</button><pre class="js-to-copy"><span class="pl-c1">npx ct --confFile=staging.courgette-conf.js</span></pre>
  </div>
                        </div>
                        <h2><a name="user-content-page-files-and-component-files" class="anchor" aria-hidden="true" href="#page-files-and-component-files"></a>.page files and .component files</h2>
                        <h3><a name="user-content-page-file-example" class="anchor" aria-hidden="true" href="#page-file-example"></a>.page file example</h3>
                        <p>Example of a page object located at <code>uiTests/pages/simple.page</code>:</p>
                        <div class="highlight highlight-source-yaml">
                            <div class="pre-wrap">
  <button type="button" class="copy-code-btn">Copy</button><pre class="js-to-copy"><span class="pl-ent">path</span>: <span class="pl-s">/simple-page</span>

<span class="pl-ent">selectors</span>:
  <span class="pl-ent">Go to home page by react router link</span>: <span class="pl-s">[data-test="go-to-home-link"]</span>
  <span class="pl-ent">some other element selected by class</span>: <span class="pl-s">.my-class</span>
  <span class="pl-ent">some other element selected by id</span>: <span class="pl-s"><span class="pl-pds">'</span>#my-id<span class="pl-pds">'</span></span>

<span class="pl-ent">XPaths</span>:
  <span class="pl-ent">main heading</span>: <span class="pl-s">//*[contains(@class, "main-heading")]</span>
  <span class="pl-ent">another element</span>: <span class="pl-s">//*[contains(@class, "something-else")]</span>

<span class="pl-ent">deepSelectors</span>:
  <span class="pl-ent">my web component</span>: <span class="pl-s">.inside-custom-web-component</span>

<span class="pl-ent">components</span>:
  - <span class="pl-s">banner</span>
  - <span class="pl-s">footer</span></pre>
  </div>
                        </div>
                        <p>A .page file is made up of:</p>
                        <ul>
                            <li>path - the page's path, navigated to when you use the <code>Given I am on the 'simple' page</code> step</li>
                            <li>selectors - css selectors, the name on the left side of the : is what you put in your gherkin steps, the selector on the right references your html element. Note if you use id selectors (with a hash) you need to put it in quotes '' or the yaml file will think it's a comment</li>
                            <li>XPaths - same as selectors but using XPath selectors instead</li>
                            <li>components - a list of components which will be loaded in from the <code>uiTests/components</code> folder and composed into the page object (see .component file example of the footer component)</li>
                        </ul>
                        <h3><a name="user-content-component-file-example" class="anchor" aria-hidden="true" href="#component-file-example"></a>.component file example</h3>
                        <p>Example of a component object located at <code>uiTests/components/footer.component</code>:</p>
                        <div class="highlight highlight-source-yaml">
                            <div class="pre-wrap">
  <button type="button" class="copy-code-btn">Copy</button><pre class="js-to-copy"><span class="pl-ent">selectors</span>:
  <span class="pl-ent">footer wrapper</span>: <span class="pl-s">.footer</span>

<span class="pl-ent">components</span>:
  - <span class="pl-s">footer-item1</span>
  - <span class="pl-s">footer-item2</span></pre>
  </div>
                        </div>
                        <p>A .component file is made up of:</p>
                        <ul>
                            <li>selectors - css selectors, the name on the left side of the : is what you put in your gherkin steps, the selector on the right references your html element. Note if you use id selectors (with a hash) you need to put it in quotes '' or the yaml file will think it's a comment</li>
                            <li>XPaths - same as selectors but using XPath selectors instead</li>
                            <li>components - a list of components which will be loaded in from the <code>uiTests/components</code> folder and composed into the component object</li>
                        </ul>
                        <p>Example site with a .page containing two site-wide components:</p>
                        <p><a target="_blank" rel="noopener noreferrer" href="https://raw.githubusercontent.com/canvaspixels/courgette/master/yaml-files.jpg"><img src="https://raw.githubusercontent.com/canvaspixels/courgette/master/yaml-files.jpg" alt=".page file with two .component files" style="max-width:100%;"></a></p>
                        <h2><a name="user-content-parallelisation" class="anchor" aria-hidden="true" href="#parallelisation"></a>Parallelisation</h2>
                        <p>By default all your .feature files will run in parallel to speed up running all your tests. This means that across your .feature files your tests should not conflict, i.e. you won't be able to do setup in one feature file such as adding a todo item to your todo app, and teardown in another .feature file such as deleting that same todo item as you'll get a race condition. A suggestion is that if you're logging into your app for example that you use a different user account for each .feature file to avoid conflicting.</p>
                        <p>If you run <code>COURGETTE_LINEARISE=1 npx ct</code> then they won't run in parallel, nor will they if you’re running just one scenario or feature with a @tag.</p>
                        <p><code>maxInstances: 4</code> in the conf can be altered depending on how much load your computer can handle.</p>
                        <p>If you've just setup Courgette with the setup script this will work, otherwise search for COURGETTE_LINEARISE inside the <a href="https://github.com/canvaspixels/courgette/blob/master/sample-courgette-conf.js" target="_blank" rel="noopener noreferrer">sample conf</a></p>
                        <h2><a name="user-content-snippets--live-templates" class="anchor" aria-hidden="true" href="#snippets--live-templates"></a>Snippets / Live templates</h2>
                        <p>Snippets are available for Sublime Text 3, Webstorm (live templates), VSCode and Atom. They are added to your IDEs automatically on OSX. If you're on other operating systems you can copy them from inside the snippets folder in this project.</p>
                        <p>To add them to your IDE run:</p>
                        <div class="pre-wrap">
  <button type="button" class="copy-code-btn">Copy</button><pre class="js-to-copy"><code>npm run setup-courgette-snippets
</code></pre>
</div>
                        <p>You may need to restart your IDE to refresh the snippets.</p>
                        <h2><a name="user-content-combining-steps" class="anchor" aria-hidden="true" href="#combining-steps"></a>Combining steps</h2>
                        <p>While the gherkin step examples in this repo are all single actions and assertions, you can easily combine a number of steps into one.</p>
                        <p>For example in a scenario that has the following steps:</p>
                        <div class="highlight highlight-text-gherkin-feature">
                            <div class="pre-wrap">
  <button type="button" class="copy-code-btn">Copy</button><pre class="js-to-copy"><span class="pl-k">Scenario</span>:<span class="pl-s"> I expect to see items in the dashboard menu</span>
  <span class="pl-k">Given </span>I am on the <span class="pl-s">'twitter login'</span> page
  <span class="pl-k">When </span>I set <span class="pl-s">'username'</span> to <span class="pl-s">'peoplesvote_uk'</span>
  <span class="pl-k">And </span>I set <span class="pl-s">'password'</span> to <span class="pl-s">'password~1'</span>
  <span class="pl-k">And </span>I submit the <span class="pl-s">'login form'</span>
  <span class="pl-k">Then </span>I expect to be on the <span class="pl-s">'dashboard'</span> page
  <span class="pl-k">When </span>I click the <span class="pl-s">'dashboard menu'</span>
  <span class="pl-k">Then </span>I expect the <span class="pl-s">'dashboard menu items'</span> to be visible</pre>
  </div>
                        </div>
                        <p>we could simplify it by combining them into one so that it is more obvious what we are intending to test:</p>
                        <div class="highlight highlight-text-gherkin-feature">
                            <div class="pre-wrap">
  <button type="button" class="copy-code-btn">Copy</button><pre class="js-to-copy"><span class="pl-k">Scenario</span>:<span class="pl-s"> I expect to see items in the dashboard menu</span>
  <span class="pl-k">Given </span>I am logged in
  <span class="pl-k">When </span>I click the <span class="pl-s">'dashboard menu'</span>
  <span class="pl-k">Then </span>I expect the <span class="pl-s">'dashboard menu items'</span> to be visible</pre>
  </div>
                        </div>
                        <p>This allows you to stop repeating yourself with the login steps, making them more reusable. Also it makes the test much more readable and focusses on the subject in test. It is a precondition that we need to be logged in and not really what we are testing. If you are in control of your mocks and are able to mock out a logged-in state, say with cookies, then that is preferable as it'll take a lot less time to run. But if you are doing system tests on a staging environment for example then you may have to login how a user would do, via the login form. Remember to keep your credentials out of your repository!</p>
                        <p>To add a <code>Given I am logged in</code> step we’ll need to create our own custom step definition. Add this to the bottom of <code>uiTests/stepDefinitions/common-step-defintions.js</code> and change the username and password twitter credentials:</p>
                        <div class="highlight highlight-source-js">
                            <div class="pre-wrap">
  <button type="button" class="copy-code-btn">Copy</button><pre class="js-to-copy"><span class="pl-en">Given</span>(<span class="pl-sr"><span class="pl-pds">/</span><span class="pl-k">^</span>I am logged in<span class="pl-k">$</span><span class="pl-pds">/</span></span>, <span class="pl-k">async</span> <span class="pl-k">function</span>() {
  <span class="pl-k">await</span> <span class="pl-c1">this</span>.<span class="pl-en">goToPage</span>(<span class="pl-s"><span class="pl-pds">'</span>twitter login<span class="pl-pds">'</span></span>);
  <span class="pl-k">await</span> <span class="pl-c1">this</span>.<span class="pl-en">setInputFieldValue</span>(<span class="pl-s"><span class="pl-pds">'</span>username<span class="pl-pds">'</span></span>, <span class="pl-s"><span class="pl-pds">'</span>peoplesvote_uk<span class="pl-pds">'</span></span>);
  <span class="pl-k">await</span> <span class="pl-c1">this</span>.<span class="pl-en">setInputFieldValue</span>(<span class="pl-s"><span class="pl-pds">'</span>password<span class="pl-pds">'</span></span>, <span class="pl-s"><span class="pl-pds">'</span>password~1<span class="pl-pds">'</span></span>);
  <span class="pl-k">await</span> <span class="pl-c1">this</span>.<span class="pl-en">submitForm</span>(<span class="pl-s"><span class="pl-pds">'</span>login form<span class="pl-pds">'</span></span>);
  <span class="pl-k">await</span> <span class="pl-c1">this</span>.<span class="pl-en">checkUrlIs</span>(<span class="pl-s"><span class="pl-pds">'</span>https://twitter.com<span class="pl-pds">'</span></span>);
});</pre>
</div>
                        </div>
                        <p>Create yourself a new feature file, let's call it <code>twitter-dashboard-menu.feature</code> and save it in the <code>uiTests/features</code> folder. Paste the following into it:</p>
                        <div class="highlight highlight-text-gherkin-feature">
                            <div class="pre-wrap">
  <button type="button" class="copy-code-btn">Copy</button><pre class="js-to-copy"><span class="pl-k">@twitter-dashboard-menu</span>
<span class="pl-k">Feature</span>:<span class="pl-s"> Test feature</span>

  <span class="pl-k">@twitter-dashboard-menu-items</span>
  <span class="pl-k">Scenario</span>:<span class="pl-s"> I expect to see items in the dashboard menu</span>
    <span class="pl-k">Given </span>I am logged in</pre>
    </div>
                        </div>
                        <p>Then run that scenario: <code>npx ct @twitter-dashboard-menu-items</code></p>
                        <p>Have a look at the <a href="/api">available methods</a> that you can use to combine your steps.</p>
                        <h2><a name="user-content-tidying-up-unused-step-definitions" class="anchor" aria-hidden="true" href="#tidying-up-unused-step-definitions"></a>Tidying up unused step definitions</h2>
                        <p>To run all your tests then print out a summary of the usage of your step definitions run:</p>
                        <div class="highlight highlight-text-shell-session">
                            <div class="pre-wrap">
  <button type="button" class="copy-code-btn">Copy</button><pre class="js-to-copy"><span class="pl-c1">npx ct --showStepDefinitionUsage</span></pre>
  </div>
                        </div>

                        <h2><a name="user-content-tidying-page-objects" class="anchor" aria-hidden="true" href="#tidying-page-objects"></a>Tidying page objects</h2>
                        <p>To test your selectors run this script in your developer console (right click your browser and inspect --&gt; console tab or f12 in some browsers)</p>
                        <div class="pre-wrap">
  <button type="button" class="copy-code-btn">Copy</button><pre class="js-to-copy"><code>checkSelectors = (selectors) =&gt; {
  const selectorsArr = selectors.split('\n');
  let selectorType = 'xpath';
  console.log('These don’t exist:')
  selectorsArr.forEach((el) =&gt; {
    if (el.toLowerCase().startsWith('selectors:')) {
      selectorType = 'css'
    }
    if (el.toLowerCase().startsWith('xpaths:')) {
      selectorType = 'css'
    }
    if (el.startsWith('  ')) {
      const selector = el.replace('  ', '')
      const match = selector.match(/(.*): (.*)/)
      const key = match[1]
      const val = match[2]

      if ((selectorType === 'css' &amp;&amp; !$$(val).length) ||
      (selectorType === 'xpath' &amp;&amp; !$x(val).length)) {
          console.log(selector)
      }
    }
  })
}

checkSelectors(\`
selectors:
  main heading: .main-heading
  proceed button: .proceed button
\`)

checkSelectors(\`
xpaths:
  main heading: //*[contains(@someattr,"attrvalue")]//*[text()="heading text"]
  proceed button: //*[text()="my text in and element deep inside my button"]/ancestor::button
\`)
</code></pre>
</div>
                      </div>
`}} />
                    </div>
      </CommonTemplate>
    );
  }
}

export default Guide;
