import React from 'react';
// import { Link } from 'react-router-dom';
import CommonTemplate from './CommonTemplate';
// import Table from './Table';

class GettingStarted extends React.Component {
  state = { showTips: false }

  render() {
    const { showTips } = this.state;
    return (
  <CommonTemplate className="">
<h1>Getting Started</h1>
<p>Courgette is a Selenium-based UI Testing Framework written in JS that’s built on top of <br />Cucumber with Protractor for desktop / hybrid apps and Cucumber with WDIO and appium for native mobile apps.</p>

<h2>Setup for Desktop / hybrid apps</h2>
<p>This assumes that you have an npm project. If you don’t then make a new one with <code className="js-to-copy">npm init</code>. It also assumes you are on a Mac, Linux or Windows and have node 8+, npm 6+, and the latest version of Chrome installed.</p>
<p>Make sure you are on a clean working copy in your version control and type this into your terminal:</p>
<div className="pre-wrap">
  <button type="button" className="copy-code-btn">Copy</button><pre><code className="js-to-copy">npm install courgette &amp;&amp;
  npx ct-install-chromedriver &amp;&amp;
  npx ct-install-sample-conf &amp;&amp;
  npx ct-install-sample-tests &amp;&amp;
  npx ct-package-scripts
</code></pre>
</div>
<p>This will install courgette, install ChromeDriver, create a <code className="js-to-copy">uiTests</code> folder with the some sample / example tests in it, an seeded <code className="js-to-copy">courgette-conf.js</code> file, adds the <code className="js-to-copy">ct</code>, <code className="js-to-copy">postinstall</code>, and <code className="js-to-copy">install-chromedriver</code> scripts to your package.json. Note the changes in your version control.</p>

<p>Optionally to install the courgette snippets/Live templates to your IDEs, to improve productivity, run the following:</p>
<div className="pre-wrap">
  <button type="button" className="copy-code-btn">Copy</button><pre><code className="js-to-copy">npx ct-setup-snippets
</code></pre>
</div>


<p>Run the sample, type into your terminal:</p>
<div className="pre-wrap">
  <button type="button" className="copy-code-btn">Copy</button><pre><code className="js-to-copy">npx ct
</code></pre></div>

<h3><a href="" onClick={(e) => { e.preventDefault(); this.setState({ showTips: !showTips }) }}>&gt; Futher tips:</a></h3>
{showTips && (
  <>
  <ol>
    <li>To improve organisation and scalability, easily compose Page Objects and Component Objects. Page Objects and Component Objects are composed of <a href="https://www.protractortest.org/#/locators" target="_blank" rel="noopener noreferrer">Locators</a>, custom methods, and other Component Objects. Components can compose Components which compose Components etc. The only difference between a Page Object and a Component Object is a Component Object does not have an URL. Use the <a href="https://github.com/canvaspixels/courgette/blob/master/STEP_DEFINITIONS.md#step-definitions" target="_blank" rel="noopener noreferrer">step definitions provided</a> (or create your own) to write your own first scenario.</li>
    <li>If you’re using git for source control, add <code className="js-to-copy">uiTestResult</code> to your .gitignore file. If I’m on a fresh project i’ll run: <code className="js-to-copy">git init &amp;&amp; BR=$'\n' &amp;&amp; echo "node_modules${'{BR}'}uiTestResult" &gt; .gitignore &amp;&amp; git add . &amp;&amp; git commit -am 'init commit'</code> at the root of my new project folder in terminal.</li>
  </ol>
  <p>As a shortcut, to create yourself a new npm project, initialise npm (create package.json), initialise git with an appropriate ignore file and create a commit, paste the following into your terminal:</p>
  <div className="pre-wrap">
    <button type="button" className="copy-code-btn">Copy</button><pre><code className="js-to-copy">mkdir yourProjectName &amp;&amp; cd $_ &amp;&amp; npm init -y &amp;&amp; git init &amp;&amp; BR=$'\n' &amp;&amp; echo "node_modules${'{BR}'}uiTestResult" &gt; .gitignore &amp;&amp; git add . &amp;&amp; git commit -am 'init commit'
  </code></pre></div>
  <p>or without the git stuff:</p>
  <div className="pre-wrap">
    <button type="button" className="copy-code-btn">Copy</button><pre><code className="js-to-copy">mkdir yourProjectName &amp;&amp; cd $_ &amp;&amp; npm init -y
  </code></pre></div>
  </>
)}

<h2>Setup for Mobile apps</h2>
<p>Setup for mobile requires some Appium and webdriverio packages. Install those by pasting this into your terminal:</p>

<div className="pre-wrap">
  <button type="button" className="copy-code-btn">Copy</button><pre><code className="js-to-copy">npm i -D
  appium
  @wdio/cucumber-framework
  @wdio/appium-service
  @wdio/cli
  @wdio/local-runner
  @wdio/spec-reporter
  @wdio/sync wdio-json-reporter
</code></pre>
</div>

<p>Install appium doctor globally to check appium is install correctly.</p>

<div className="pre-wrap">
  <button type="button" className="copy-code-btn">Copy</button><pre><code className="js-to-copy">npm install -g appium-doctor</code></pre>
</div>

  <p>Once appium looks good to go, paste the following to setup with Courgette</p>

<div className="pre-wrap">
  <button type="button" className="copy-code-btn">Copy</button><pre><code className="js-to-copy">npm install courgette &amp;&amp;
  npx ct-install-sample-mobile-conf &amp;&amp;
  npx ct-package-scripts-mobile &amp;&amp;
  npx ct-install-sample-tests
</code></pre>
</div>

<p>Optionally to install the courgette snippets/Live templates to your IDEs, to improve productivity, run the following:</p>
<div className="pre-wrap">
  <button type="button" className="copy-code-btn">Copy</button><pre><code className="js-to-copy">npx ct-setup-snippets
</code></pre>
</div>

<p>Now you’re setup you can get writing and run your tests (see examples in the uiTests folder generated from the previous command):</p>

<div className="pre-wrap">
  <button type="button" className="copy-code-btn">Copy</button><pre><code className="js-to-copy">npx ct --confFile=courgette-mobile-conf.js</code></pre>
</div>

<p>You can just run one feature or scenario if you wish.</p>

<div className="pre-wrap">
  <button type="button" className="copy-code-btn">Copy</button><pre><code className="js-to-copy">npx ct --confFile=courgette-mobile-conf.js --tags=@login</code></pre>
</div>

<p>You can also specify the platform.</p>

<div className="pre-wrap">
  <button type="button" className="copy-code-btn">Copy</button><pre><code className="js-to-copy">PLATFORM=ios-dev npx ct --confFile=courgette-mobile-conf.js --tags=@login</code></pre>
</div>

  </CommonTemplate>
    )
  }
};


export default GettingStarted;
