import React from 'react';
import { NavLink } from 'react-router-dom';
import Cookies from 'js-cookie'
// import FooterItem1 from './FooterItem1';
// import FooterItem2 from './FooterItem2';
import './footer.scss';
import utils from './utils'

const Footer = () => (
  <div className="footer">
    <div className="footer__inner">
      <ul>
        <li><NavLink to="/getting-started">Getting Started</NavLink></li>
        <li><NavLink to="/faqs">FAQs</NavLink></li>
        <li><NavLink to="/privacy-policy">Privacy Policy</NavLink></li>
      </ul>
    </div>
    { !Cookies.get('MCPopupSubscribed') && (
      <button className="subscribe-btn" type="button" onClick={utils.subscribe}>Subscribe</button>
    )}
  </div>
);

export default Footer;
