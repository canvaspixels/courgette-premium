import React from 'react';
// import { Link } from 'react-router-dom';
import Nav from './Nav';
import Footer from './Footer';

const CommonTemplate = ({ children, className, addContainer, nav, footer }) => (
  <div className="main-wrap">
    { nav || <Nav /> }
    {addContainer ? (
      <div className={`${className} container`}>
        <div className="container__inner">
          { children }
        </div>
      </div>
    ) : (
      <div className={className || 'main-container'}>
        { children }
      </div>
    )}
    { footer || <Footer /> }
  </div>
);

export default CommonTemplate;
