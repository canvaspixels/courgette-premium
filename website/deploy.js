var FtpDeploy = require('ftp-deploy');
var ftpJSON = require('./.vscode/ftp-sync.json');

var ftpDeploy = new FtpDeploy();

var config = {
    host: ftpJSON.host,
    user: ftpJSON.username,                   // NOTE that this was username in 1.x 
    password: ftpJSON.password,           // optional, prompted if none given
    port: 21,
    localRoot: __dirname + '/build',
    remoteRoot: '/build/',
    include: '**/*',      // this would upload everything except dot files
    // include: ['*.php', 'dist/*'],
    include: ['**/*'],
    exclude: [],     // e.g. exclude sourcemaps - ** exclude: [] if nothing to exclude **
    deleteRemote: false,              // delete ALL existing files at destination before uploading, if true
    forcePasv: false,                 // Passive mode is forced (EPSV command is not sent)
}

// ftpDeploy.on('uploading', function(data) {
//     data.totalFilesCount;       // total file count being transferred
//     data.transferredFileCount; // number of files transferred
//     data.filename;             // partial path with filename being uploaded
// });
ftpDeploy.on('uploaded', function(data) {
    console.log(data);         // same data as uploading event
});
ftpDeploy.on('log', function(data) {
    console.log(data);         // same data as uploading event
});
 
// use with promises
// ftpDeploy.deploy(config)
//     .then(res => console.log('finished:', res))
//     .catch(err => console.log(err))
    
// use with callback
ftpDeploy.deploy(config, function(err, res) {
    if (err) console.log(err)
    else console.log('finished:', res);
});