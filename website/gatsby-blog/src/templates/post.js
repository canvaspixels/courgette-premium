import React from "react"
import { graphql } from "gatsby"
import CommonTemplate from '../components/CommonTemplate';
import '../../../src/index.scss';
import './blog-post.scss';
import SEO from "../components/seo"

export default ({ data }) => {
  const post = data.markdownRemark
  return (
    <CommonTemplate addContainer>
      <SEO
        title={post.frontmatter.title}
        description={post.frontmatter.description}
        metaURL={`/blog${post.fields.slug}`} 
        metaImgSrc={`/blog/static/i${post.frontmatter.imgSrc}?v=${Date.now()}`}
      />
      <article itemScope itemType="http://schema.org/BlogPosting" className="blog-post">
        <h1 itemProp="name">{post.frontmatter.title}</h1>
        <small><time dateTime={post.frontmatter.datetime} itemProp="datePublished">{post.frontmatter.date}</time></small>
        <div itemProp="articleBody" dangerouslySetInnerHTML={{ __html: post.html }} />
      </article>
    </CommonTemplate>
  )
}
export const query = graphql`
  query($slug: String!) {
    markdownRemark(fields: { slug: { eq: $slug } }) {
      html
      frontmatter {
        title
        imgSrc
        description
        date(formatString: "DD MMMM, YYYY")
        datetime: date(formatString: "YYYY-MM-DD")
      }
      fields {
        slug
      }
    }
  }
`