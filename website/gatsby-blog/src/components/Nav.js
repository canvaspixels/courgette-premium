import React, { Component } from 'react';
// import { withRouter } from 'react-router-dom';

import '../../../src/nav.scss';

class Nav extends Component {
  state = {}
  // componentDidMount() {
  //   this.props.history.listen((location, action) => {
  //       // location is an object like window.location
  //       console.log(action, location.pathname, location.state)
  //   });
  // }

  render() {
    const { showMenu } = this.state;
    return (
      <div className={`nav ${showMenu && 'show'}`}>
        <header>
          <div className="nav__header-left">
            <button type="button" onClick={() => this.setState({showMenu: true})} className="burger">Menu</button>
            <p className="logo">
              <a href="/">Courgette <span className="logo__sub">Testing</span></a>
            </p>
          </div>
          <ul>
            <li><a href="/getting-started">Getting Started</a></li>
            <li><a href="/guide">Guide</a></li>
            <li><a href="/step-definitions">Step Definitions</a></li>
            <li><a href="/api">API</a></li>
            <li><a href="/blog/">Blog</a></li>
            <li><a href="https://github.com/canvaspixels/courgette" target="_blank" rel="noopener noreferrer">Github</a></li>
          </ul>
        </header>
        <div className="overlay" onClick={() => this.setState({showMenu: false})}>
          <div className="overlay-inner" />
        </div>
        <div className="main-nav">
          <button type="button" className="close-btn" onClick={() => this.setState({showMenu: false})}>Close</button>
          <div className="main-nav__inner">
            <ul>
              <li><a href="/" exact="true">Overview</a></li>
              <li><a href="/getting-started">Getting Started</a></li>
              <li><a href="/guide">Guide</a></li>
              <li><a href="/step-definitions">Step Definitions</a></li>
              <li><a href="/api">API</a></li>
              <li><a href="/faqs">FAQs</a></li>
              <li><a href="/bdd">BDD - User Stories</a></li>
              <li><a href="/blog/">Blog</a></li>
              <li><a href="https://github.com/canvaspixels/courgette" target="_blank" rel="noopener noreferrer">Github</a></li>
            </ul>
          </div>
        </div>
      </div>
    )
  }
}

export default Nav;
