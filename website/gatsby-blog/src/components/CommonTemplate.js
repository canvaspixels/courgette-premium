import React from 'react';
// import { Link } from 'react-router-dom';
import Nav from './Nav';
import Footer from './Footer';
import Cookies from 'js-cookie'

import utils from '../../../src/utils'
import GDPR from './GDPR'


// const copyToClipboard = str => {
//   const el = global.document.createElement('textarea');
//   el.value = str;
//   global.document.body.appendChild(el);
//   el.select();
//   global.document.execCommand('copy');
//   global.document.body.removeChild(el);
// };

// global.document.body.addEventListener('click', (ev) => {
//   if (ev.target.classList.contains('copy-code-btn')) {
//     const el = ev.target;
//     el.innerHTML = 'Copied &#x2713';
//     const text = el.parentElement.querySelector('.js-to-copy').innerText;
//     copyToClipboard(text);
//     setTimeout(() => { el.innerHTML = 'Copy' }, 5000);
//   }
// });



const CommonTemplate = ({ children, className, addContainer, nav, footer }) => (
  <div className="main-wrap">
    { nav || <Nav /> }
    {addContainer ? (
      <div className={`${className} container`}>
        <div className="container__inner">
          { children }
        </div>
      </div>
    ) : (
      <div className={className || 'main-container'}>
        { children }
      </div>
    )}
    { footer || <Footer /> }

    { !Cookies.get('MCPopupSubscribed') ? (
      <button className="subscribe-btn" type="button" onClick={utils.subscribe}>Subscribe</button>
    ) : null}

    <GDPR />
  </div>
);

export default CommonTemplate;
