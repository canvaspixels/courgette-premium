import React from 'react';
// import FooterItem1 from './FooterItem1';
// import FooterItem2 from './FooterItem2';
import '../../../src/footer.scss';


const Footer = () => (
  <div className="footer">
    <div className="footer__inner">
      <ul>
        <li><a href="/getting-started">Getting Started</a></li>
        <li><a href="/faqs">FAQs</a></li>
        <li><a href="/privacy-policy">Privacy Policy</a></li>
      </ul>
    </div>
  </div>
);

export default Footer;
