import React from 'react'
// import React, { useState } from 'react'

import Cookies from 'js-cookie'

// export default function GDPR() {
//   const [isPolicyAgreed, setPolicy] = useState(!!Cookies.get('privacyPolicyAgreed'));
//   console.log('isPolicyAgreed', isPolicyAgreed);
//   const displayProp = isPolicyAgreed ? 'none' : 'block'
//   console.log('displayProp', displayProp);
export default class GDPR extends React.Component {
  state = { isPolicyAgreed: true }

  componentDidMount() {
    setTimeout(() => {
      this.setState({ isPolicyAgreed: !!Cookies.get('privacyPolicyAgreed') })
    }, 1000)
  }
  // const [isPolicyAgreed, setPolicy] = useState(!!Cookies.get('privacyPolicyAgreed'));
  render() {
    // console.log('isPolicyAgreed', isPolicyAgreed);
    // console.log('this.state.isPolicyAgreed', this.state.isPolicyAgreed);
    // return (
    //   <div>
    //   <button type="button" onClick={() => {
    //             {/*Cookies.set('privacyPolicyAgreed', 'true')*/}
    //             // setPolicy(true)
    //             this.setState({ isPolicyAgreed: !this.state.isPolicyAgreed })
    //           }}>OK, I Understand</button>
    //   { <div ><button type="button" onClick={() => {
    //             {/*Cookies.set('privacyPolicyAgreed', 'true')*/}
    //             // setPolicy(true)
    //             this.setState({ isPolicyAgreed: !this.state.isPolicyAgreed })
    //           }}>OK, I Understand</button>aaa</div>}

    //   </div>
    // )
    return (
      <div className="gdpr-banner-wrapper" style={{display: this.state.isPolicyAgreed ? 'none' : 'block'}}>
        <div className="gdpr-banner">
          <h2 className="gdpr-banner-title">We’ve changed our Privacy and Cookie Policy</h2>

          <p className="gdpr-banner-content">Our Privacy and Cookie Policy has changed. By continuing to use this site without changing your cookie settings, we assume that you're comfortable with this.</p>

          <ul className="gdpr-banner-options">
            <li>
              <button type="button" className="gdpr-ok-button" onClick={() => {
                this.setState({ isPolicyAgreed: true })
                Cookies.set('privacyPolicyAgreed', 'true')
                // setPolicy(true)
              }}>OK, I Understand</button>
            </li>
            <li>
              <a href="/privacy-policy">Read our Privacy Policy</a>
            </li>
          </ul>
        </div>
      </div>
    )
  }
}

