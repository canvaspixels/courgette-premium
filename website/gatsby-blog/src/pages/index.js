import React from "react"
import { Link, graphql } from "gatsby"
import CommonTemplate from '../components/CommonTemplate';
import '../../../src/index.scss';
// import Image from "../components/image"
import SEO from "../components/seo"

const IndexPage = ({ data }) => {
  return (
    <CommonTemplate addContainer>
      <SEO title="Home" />
      <h1>Welcome to the Courgette Testing Blog</h1>
      <p>Put your slippers on, grab a cuppa, and get comfortable!</p>
      {/* <Link to="/page-2">Go to page 2</Link> */}

      <div>
        {/* <h4>{data.allMarkdownRemark.totalCount} Posts</h4> */}
        {data.allMarkdownRemark.edges
          .filter(({ node }) => node.frontmatter.draft !== 'true')
          .map(({ node }) => (
            <div key={node.id}>
              <h3
              >
                <Link
                  to={node.fields.slug}
                >
                  {node.frontmatter.draft}
                  {node.frontmatter.title}{" "}
                </Link>
                <br/>
                <small>{node.frontmatter.date}</small>
              </h3>
              <p>{node.excerpt}</p>
            </div>
          ))}
        </div>
    </CommonTemplate>
  )
}

export default IndexPage

export const query = graphql`
  query {
    allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC }) {
      totalCount
      edges {
        node {
          id
          frontmatter {
            title
            draft
            date(formatString: "DD MMMM, YYYY")
          }
          fields {
            slug
          }
          excerpt
        }
      }
    }
  }
`