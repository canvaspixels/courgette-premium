---
title: "Gotchas when using UI test frameworks or end to end solutions"
date: "2019-01-01"
draft: "true"
---

false positives
false negatives
make sure front end can call its services, test with cors endpoints if applicable, potentially use mocks (recommend frameworks)
  set up smoke tests
multiple accounts

CI integration
- domcontentloaded - outside access to the web, 3rd party scripts
- remote debugging
- protractor console output