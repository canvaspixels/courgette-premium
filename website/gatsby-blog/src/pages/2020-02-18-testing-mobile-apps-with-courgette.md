---
title: "Testing native mobile applications with Courgette"
date: "2020-02-18"
description: "Courgette now works with Appium and Webdriverio"
imgSrc: "/mobile-courgette.jpg"
---

Guess what??? Courgette now has the ability to test your Android and iOS applications by harnessing the power of Webdriverio and Appium.

<img src="/blog/static/i/mobile-courgette.jpg" alt="Courgette Mobile" />

It has a new set of [mobile specific step definitions](https://courgette-testing.com/mobile-step-definitions) which you can use to get going writing scenarios that actually test your app, whether it be on a simulator or against real devices either locally or on browser stack / sauce labs / aws device farm etc. Head over to the [Getting Started](https://courgette-testing.com/getting-started) and scroll to **Setup for Mobile apps**.

Regarding locator strategies, with the advice of [WDIO](https://webdriver.io/docs/selectors.html) and [Appium](https://appiumpro.com/editions/20) at time of writing Courgette will only find elements by accessibility id... so no classes or xpaths. This may change if anybody has a use case for it and wants to take it on.

This means that for React Native apps you’ll need to do something like the following:

```
<Button accessibilityLabel="Submit Button" testID="Submit Button"></Button>
```

Or if we are being good and considering those using voiceover on ios / talkback on Android...

```
<Button accessibilityLabel={process.env.IS_TEST_APP ? 'Submit Button' : 'Actual label for accessibility used in production build if this is different from the "Submit Button"'} testID="Submit Button"></Button>
```

Or write yourself a function to spread these props to remove the duplication. The drawback of this is that you'll need to import that function in each component. To keep things as simple as possible, use a descriptive testID that’s what you'd expect would be best for Voiceover / Talkback users. For more info read [Accessibility in the React Native docs](https://facebook.github.io/react-native/docs/accessibility).

At the time of writing this, the use of [.page files](https://courgette-testing.com/guide#user-content-page-files-and-component-files) is mandatory but this may change soon so that you can just use the accessibility id straight in the `.feature` file.

So currently to write an automated test all you need for example is a file called `help.feature` containing:

```
@help
Feature: help screen tests

  @help-screen
  Scenario: Help screen
    Given I am on the 'landing' screen
    When I tap the 'help button'
    Then I expect to be on the 'help' page
    And take a screenshot called 'help-screen'
```

... a file called `landing.page` containing:

```
path: LandingScreen

selectors:
  help button: HelpButton
```

... and a file called `help.page` containing:

```
path: HelpScreen
```

And that's all there is to it!