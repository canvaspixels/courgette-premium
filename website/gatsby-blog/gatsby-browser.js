/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */
import utils from '../src/utils'
// You can delete this file if you're not using it
export function onClientEntry() {
  utils.addMailChimpScript(utils.showSubscribeSetCookie)
}